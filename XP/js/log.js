var user_id = 0;
var app, db, storage;
var logs = [];
var task_id;
var event_id;

function add_item(table, item, callback) {
    // Add a second document with a generated ID.
    db.collection(table).add(item).then(function(docRef) {
        console.log("Document written with ID: ", docRef.id);
        callback();
    }).catch(function(error) {
        console.error("Error adding document: ", error);
        callback();
    });
}

function read_id(callback) {
    db.collection('user_id').get().then(res => {
        for (let k = 0; k < res.docs.length; k++) {
            user_id = Math.max(res.docs[k].data().id, user_id);
        }
        user_id += 1;
        if (user_id%2 == 0) { user_id += 1; }
        callback();
    });
}

function init_log() {

    if (DEBUG) {
        user_id = 1000;
        task_id = 0;
        return;
    }

    var firebaseConfig = {
        apiKey: "AIzaSyDjoTF77_OIfngYUHbpd5dZM9UnbDldp9g",
        authDomain: "dragndroplog.firebaseapp.com",
        databaseURL: "https://dragndroplog.firebaseio.com",
        projectId: "dragndroplog",
        storageBucket: "dragndroplog.appspot.com",
        messagingSenderId: "895116066587",
        appId: "1:895116066587:web:06a0b72cd4ae78eb899dbe",
        measurementId: "G-ZXY64ZG4BF"
    };
    // Initialize Firebase
    app = firebase.initializeApp(firebaseConfig);
    firebase.analytics();
    db = firebase.firestore();
    storage = firebase.storage().ref();

    read_id(function() {
        console.log('got user ! ', user_id);
        add_item('user_id', {id: user_id}, function() { console.log('User id updated in firebase.'); });
    })

    task_id = 0;
}
function log_storyphase(phase) {
    // console.log(phase);
    logs.push({
        'type':      'storyphase',
        'timestamp': Date.now(),
        'value':     phase
    })
}

function log_demographics(age, gender, compuse, gameuse, pointing) {
    // console.log(age, gender, compuse, gameuse, pointing);
    logs.push({
        'type':      'demographics',
        'timestamp': Date.now(),
        'value':     {
            'age':      age,
            'gender':   gender,
            'compuse':  compuse,
            'gameuse':  gameuse,
            'pointing': pointing
        }
    })
}

function log_new_trial(task) {
    // if (task.type == 'test') { return; }
    // console.log('trial start');
    event_id = 0;

    logs.push({
        'type':      'newtrial',
        'timestamp': Date.now(),
        'value':     {
            'task':  { 'type': task.type, 'target': task.value.target.target },
            'dwell': parameters.dwell,
            'id':    task_id,
            'eventid': event_id
        }
    })
    event_id = 1;
}

function log_in_trial(type, data) {
    // console.log(type, data);
    switch(type) {
        // case 'on_nonexp_folder':
        // case 'box_drop':
        // case 'in_folder':
        // case 'exp_folder':
        // case 'box_picked':
        // case 'box_cancelled':
        // case 'out_folder':
        //     console.log(type, data);
        //     break;
        // case 'box_move':
        //     return;
        // default:
        //     console.log('HEY !!', type);
        //     break;
    }
    // if (cur_task.type == 'test') { return; }
    // console.log('in trialing...');
    logs.push({
        'type':      'intrial',
        'timestamp': Date.now(),
        'value':     {
            'id':      task_id,
            'eventid': event_id,
            'type':    type,
            'data':    data
        }
    })

    event_id += 1;
}

function log_trial_done(task) {
    // if (task.type == 'test') { return; }
    // console.log('trial done');
    logs.push({
        'type':      'endtrial',
        'timestamp': Date.now(),
        'value':     {
            'id':    task_id,
            'eventid': event_id
        }
    })
    task_id += 1;
}

function log_questionnaire(preference, why) {
    // console.log(preference);
    logs.push({
        'type':      'questionnaire',
        'timestamp': Date.now(),
        'value':     {
            'preference': preference,
            'why': why
        }
    })
}

function log_slider(value) {
    // console.log(value);
    logs.push({
        'type':      'slider',
        'timestamp': Date.now(),
        'value':     value
    })
}

function log_comment(comment) {
    // console.log(comment);
    logs.push({
        'type':      'comment',
        'timestamp': Date.now(),
        'value':     comment
    })
}

function log_submit() {
    if (DEBUG) {
        console.log(logs);
        return;
    }
    // add_item('user_id', {id: user_id}, function() { console.log('User id updated in firebase.'); });
    // add_item('logs', { 'user': user_id, 'logs': logs }, function() { console.log('logs added in firebase'); });

    var filref = storage.child(`user_${user_id}.json`);
    filref.putString(JSON.stringify({ 'user': user_id,
                                      'browser': window.navigator.userAgent,
                                      'zone': `${Intl.DateTimeFormat().resolvedOptions().timeZone}`,
                                      'logs': logs })).then(function(snapshot) {
        console.log('Logs uploaded.');
    });
    // console.log(logs);
}