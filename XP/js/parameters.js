var parameters = {};

parameters.get_label = function (level) {
    let first  = Math.floor(level / 100);
    let second = Math.floor((level - first*100) / 10);
    let third  = (level - first*100 - second*10);
    if (third == 0) {
        if (second == 0) {
            return `${first}`;
        } else {
            return `${first}.${second}`;
        }
    }
    return `${first}.${second}.${third}`;
}

function get_nb_sublevels(k2) {
    switch(k2) {
        case 10:
            return 4;
        case 20:
            return 3;
        case 30:
            return 2;
        case 40:
            return 1;
        case 50:
            return 5;
    }
}

parameters.dragndrop_list = function (type, target) {
    let h = 20;
    let w = 210;
    let ox = 5;
    let oy = ox+h+5;

    let levels = [];
    let targets = [];
    
    switch(type) {
        case 'xp':
            for (let k1 = 100; k1 <= 1600; k1 += 100) {
                levels.push(k1);
                for (let k2 = 10; k2 <= 50; k2 += 10) {
                    levels.push(k1+k2);
                    let nbsubs = get_nb_sublevels(k2);
                    // for (let k3 = 1; k3 <= 5; k3 += 1) {
                    for (let k3 = 1; k3 <= nbsubs; k3 += 1) {
                        levels.push(k1+k2+k3);
                        targets.push(k1+k2+k3);
                    }
                }
            }
            break;
        case 'fitts':
            for (let k = 100; k <= 1600; k += 100) {
                levels.push(k);
                targets.push(k);
            }
            break;
    }

    // let target = targets[Math.floor(targets.length * Math.random())];
    let label = parameters.get_label(target);
    let box = { 'id': 1, 'level': 1, 'opos': [ox, ox], 'pos': [ox, ox], 'size': [w, h], 'label': `drop in ${label}`, 'target': target };

    let items = [];
    for (let k = 0; k < levels.length; k++) {
        let label = parameters.get_label(levels[k]);
        let depth = label.split('.');
        let item = {
            'id': levels[k], 'exp': true, 'hierachy': depth.length, 'goto':2,
            'opos': [ox, (parseInt(depth[0])-1)*h+oy], 'pos': [ox, (parseInt(depth[0])-1)*h+oy],
            'size': [w, h], 'label': label, 'sublist': []
        };
        switch (depth.length) {
            case 1:
                if (type == 'xp') { item.exp = false; }
                items.push(item);
                break;
            case 2:
                if (type == 'xp') { item.exp = false; }
                items[(parseInt(depth[0])-1)].sublist.push(item);
                // items[(parseInt(depth[0])-1)].exp = false;
                break;
            case 3:
                items[(parseInt(depth[0])-1)].sublist[(parseInt(depth[1])-1)].sublist.push(item);
                // items[(parseInt(depth[0])-1)].sublist[(parseInt(depth[1])-1)].exp = false;
        }
    }

    return { 'target' : box, 'list' : items }
}

parameters.get_task = function() {
    if (parameters.current >= parameters.tasks.length) { return null; }
    let task = null;
    if (parameters.current == -1) {
        task = parameters.tasks[Math.floor(Math.random()*parameters.tasks.length)];
        document.getElementById('counter').innerHTML = ``;
    } else {
        task = parameters.tasks[parameters.current];
        parameters.current = parameters.current + 1;
        document.getElementById('counter').innerHTML = `${parameters.current}/${parameters.tasks.length}`;
    }
    return JSON.parse(JSON.stringify(task));
}

parameters.set_fitts_tasks = function() {
    parameters.tasks = [];

    let odds = [100,300,500,700,900,1100,1300,1500];
    odds = shuffle(odds);

    for (let k = 0; k < 4; k++) {
        parameters.tasks.push({ 'type': 'fitts', 'dwell': parameters.dwell, 'value': parameters.dragndrop_list('fitts', odds[k]) })
    }
    
    let evens_1 = [200,400,600,800,1000,1200,1400,1600];
    evens_1 = shuffle(evens_1);
    let evens_2 = [200,400,600,800,1000,1200,1400,1600];
    evens_2 = shuffle(evens_2);
    let evens = evens_1.concat(evens_2);

    for (let k = 0; k < evens.length; k++) {
        parameters.tasks.push({ 'type': 'fitts', 'dwell': parameters.dwell, 'value': parameters.dragndrop_list('fitts', evens[k]) })
    }

    parameters.current = 0;
    if (DEBUG) { parameters.current = parameters.tasks.length-1; }
}

parameters.set_xp_example_tasks = function() {
    parameters.tasks = [];

    let odds = [155,355,555,755,955,1155,1355,1555];
    odds = shuffle(odds);

    for (let k = 0; k < 4; k++) {
        parameters.tasks.push({ 'type': 'xp', 'dwell': parameters.dwell, 'value': parameters.dragndrop_list('xp', odds[k]) })
    }

    parameters.current = 0;
    if (DEBUG) { parameters.current = parameters.tasks.length-1; }
}

parameters.set_xp_tasks = function() {
    parameters.tasks = [];

    let targets = [355,455,555,655,755,855,955,1055,1155,1255,1355,1455];
    targets = shuffle(targets);

    for (let k = 0; k < targets.length; k++) {
        parameters.tasks.push({ 'type': 'xp', 'dwell': parameters.dwell, 'value': parameters.dragndrop_list('xp', targets[k]) })
    }

    parameters.current = 0;
    if (DEBUG) { parameters.current = parameters.tasks.length-1; }
}

parameters.set_sandbox = function(limit = -1) {
    in_sandbox = true;
    at_least_once = false;

    parameters.tasks = [];

    let targets = [355,455,555,655,755,855,955,1055,1155,1255,1355,1455];
    targets = shuffle(targets);

    for (let k = 0; k < targets.length; k++) {
        parameters.tasks.push({ 'type': 'test', 'value': parameters.dragndrop_list('xp', targets[k]) })
    }

    parameters.current = -1;
    if (limit > 0) {
        if (parameters.tasks.length - limit >= 0) {
            parameters.current = 0;
            parameters.tasks = parameters.tasks.slice(0,limit)
        }
    }
}

var colors = {
    butlabel: '#FFFFFF',
    button:   '#2C66D2',

    inzone:       '#EEEEEE',
    inzoneblink:  '#5CA7EA',
    outzone:      '#7D7D7D',
    outzoneblink: '#3D3D9D',

    label: '#000000',
    boxlabel: '#FFFFFF',
    
    inbox:     '#2154AE',
    outbox:    '#242424',
    activeinbox: '#2154AEBB',
    activeinrightbox: '#68DC72BB',
    activeoutbox: '#242424BB',

    done: '#02881f',
    background: 'white'
}