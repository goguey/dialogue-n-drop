function get_radio_value(name) {
    let a = document.getElementsByName(name)[0].checked;
    let b = document.getElementsByName(name)[1].checked;
    if (a) { return 'A'; }
    if (b) { return 'B'; }
    return '';
}

function init_story() {
    document.getElementById('task').style.display = 'none';
    document.getElementById('instruction_fitts').style.display = 'none';
    document.getElementById('slider').style.display = 'none';
    document.getElementById('instruction_xp').style.display = 'none';
    document.getElementById('instruction_xp_A_B').style.display = 'none';
    document.getElementById('done_instruction_xp_A_B').style.display = 'none';
    document.getElementById('instruction_xp_A').style.display = 'none';
    document.getElementById('instruction_xp_B').style.display = 'none';
    document.getElementById('questionnaire').style.display = 'none';
    document.getElementById('thereveal').style.display = 'none';
    document.getElementById('done_thereveal').style.display = 'none';
    document.getElementById('comments').style.display = 'none';
    document.getElementById('thanks').style.display = 'none';

    log_storyphase('introduction');

    document.getElementById('done_introduction').addEventListener('click', function () {
        if (!DEBUG) {
            let age = document.getElementById('age').value;
            let gender = document.getElementById('gender').value;
            let compuse = document.getElementById('compuse').value;
            let gameuse = document.getElementById('gameuse').value;
            let pointing = document.getElementById('pointing').value;

            this.innerHTML = 'Start Experiment (<strong>please fill in all the fields</strong>)'

            if (age == '') { return; }
            if (gender == '') { return; }
            if (compuse == '') { return; }
            if (gameuse == '') { return; }
            if (pointing == '') { return; }
            log_demographics(age, gender, compuse, gameuse, pointing);
        }

        document.getElementById('introduction').style.display = 'none'
        document.getElementById('instruction_fitts').style.display = null;

        log_storyphase('instruction_fitts');
    })

    document.getElementById('done_instruction_fitts').addEventListener('click', function () {
        document.getElementById('instruction_fitts').style.display = 'none';
        document.getElementById('task').style.display = null;

        log_storyphase('fitts');

        parameters.dwell = 1000;
        parameters.type = 'fitts';
        parameters.storypoint = 1;

        document.getElementById('system').innerHTML = 'Trials';
        parameters.set_fitts_tasks()
        init_task();
    })
}

function task_done() {
    switch (parameters.storypoint) {
        case 1:
            document.getElementById('task').style.display = 'none';
            document.getElementById('instruction_xp').style.display = null;

            log_storyphase('instruction_xp');

            document.getElementById('done_instruction_xp').addEventListener('click', function () {
                document.getElementById('instruction_xp').style.display = 'none';
                document.getElementById('task').style.display = null;


                parameters.dwell = training_dwell;
                parameters.type = 'xp';
                parameters.storypoint = 2;

                document.getElementById('system').innerHTML = 'Training';
                parameters.set_xp_example_tasks()
                init_task();
            })
            break;
        case 2:
            if (in_sandbox) { return; }

            parameters.defineAdaptativeDwell();

            document.getElementById('task').style.display = 'none';
            
            document.getElementById('instruction_xp_A_B').style.display = null;
            // document.getElementById('instruction_xp_A').style.display = null;
            // document.getElementById('task').style.display = null;
            
            document.getElementById('canvas').className = 'test';
            document.getElementById('system').innerHTML = 'Training System A';
            document.getElementById('system').parentElement.className = 'test'
            parameters.dwell = (AdFi? adapt_dwell : fixed_dwell);
            
            log_storyphase('instruction_xp_A');
            parameters.set_sandbox(5);
            init_task();
            document.getElementById('done_instruction_xp_A').style.display = 'none';


            document.getElementById('done_instruction_xp_A').addEventListener('click', function () {
                in_sandbox = false;

                document.getElementById('instruction_xp_A').style.display = 'none';
                document.getElementById('task').style.display = null;
                document.getElementById('canvas').className = '';
                document.getElementById('system').parentElement.className = ''

                log_storyphase('xp_A');

                parameters.dwell = (AdFi? adapt_dwell : fixed_dwell);
                parameters.type = 'xp';
                parameters.storypoint = 3;

                document.getElementById('system').innerHTML = 'System A';
                parameters.set_xp_tasks()
                init_task();
            })
            break;
        case 3:
            if (in_sandbox) { return; }

            document.getElementById('task').style.display = 'none';
            document.getElementById('instruction_xp_B').style.display = null;
            document.getElementById('task').style.display = null;
            document.getElementById('canvas').className = 'test';
            document.getElementById('system').innerHTML = 'Training System B';
            document.getElementById('system').parentElement.className = 'test'
            parameters.dwell = (AdFi? fixed_dwell : adapt_dwell);
            
            log_storyphase('instruction_xp_B');
            parameters.set_sandbox(5)
            init_task();
            document.getElementById('done_instruction_xp_B').style.display = 'none';


            document.getElementById('done_instruction_xp_B').addEventListener('click', function () {
                in_sandbox = false;

                document.getElementById('instruction_xp_B').style.display = 'none';
                document.getElementById('task').style.display = null;
                document.getElementById('canvas').className = '';
                document.getElementById('system').parentElement.className = ''

                log_storyphase('xp_B');

                parameters.dwell = (AdFi? fixed_dwell : adapt_dwell);
                parameters.type = 'xp';
                parameters.storypoint = 4;

                document.getElementById('system').innerHTML = 'System B';
                parameters.set_xp_tasks()
                init_task();
            })
            break;
        case 4:
            document.getElementById('task').style.display = 'none';
            document.getElementById('questionnaire').style.display = null;

            log_storyphase('questionnaire');

            document.getElementById('done_questionnaire').addEventListener('click', function () {
                let preference = get_radio_value('preference');
                let why = document.getElementById('why').value;
        
                this.innerHTML = 'Go on to the final stage (<strong>please fill up all the fields</strong>)'
        
                if (preference == '') { return; }
                log_questionnaire(preference, why);
        
                set_test();
            })
            break;
        case 5:
            document.getElementById('comments').style.display = null;
            document.getElementById('task').style.display = null;
            document.getElementById('slider').style.display = null;

            document.getElementById('systams').innerHTML = parseInt((AdFi? adapt_dwell : fixed_dwell)).toFixed(0);
            document.getElementById('systbms').innerHTML = parseInt((AdFi? fixed_dwell : adapt_dwell)).toFixed(0);;
            document.getElementById('systcms').innerHTML = parameters.dwell;

            log_storyphase('comment');

            document.getElementById('canvas').className = 'test';

            document.getElementById('system').innerHTML = 'Sandbox';
            document.getElementById('system').parentElement.className = 'test'
            parameters.set_sandbox()
            init_task();

            document.getElementById('done_comments').addEventListener('click', function () {
                let comment = document.getElementById('free').value;

                this.innerHTML = 'Next (<strong>please provide a comment</strong>)'
                if (comment == '') { return; }

                document.getElementById('comments').style.display = 'none';
                document.getElementById('task').style.display = 'none';
                document.getElementById('slider').style.display = 'none';
                document.getElementById('thanks').style.display = null;
                document.getElementById('thanks').innerHTML += '<p class="text">Experiment Code: <strong>ACAG21dndEX-ADAPT-0201</strong>.<br>Please return to Mechanical Turk and submit this experiment code.</p>'

                log_comment(comment);

                log_storyphase('end');
                log_submit();
            })
            break;
    }
}

function set_test() {
    if (DEBUG) {
        document.getElementById('introduction').style.display = 'none'
        document.getElementById('task').style.display = 'none';
        document.getElementById('instruction_fitts').style.display = 'none';
        document.getElementById('slider').style.display = 'none';
        document.getElementById('instruction_xp').style.display = 'none';
        document.getElementById('instruction_xp_A_B').style.display = 'none';
        document.getElementById('done_instruction_xp_A_B').style.display = 'none';
        document.getElementById('instruction_xp_A').style.display = 'none';
        document.getElementById('instruction_xp_B').style.display = 'none';
        document.getElementById('questionnaire').style.display = 'none';
        document.getElementById('thereveal').style.display = 'none';
        document.getElementById('done_thereveal').style.display = 'none';
        document.getElementById('comments').style.display = 'none';
        document.getElementById('thanks').style.display = 'none';
    }

    document.getElementById('questionnaire').style.display = 'none';
    document.getElementById('thereveal').style.display = null;
    document.getElementById('task').style.display = null;
    document.getElementById('slider').style.display = null;
    slider_changed = false;

    log_storyphase('sandbox');

    let val_A = (AdFi? adapt_dwell : fixed_dwell)
    document.getElementById('vlabA').style.left = `calc(${val_A/15}% + 8px)`;
    let val_B = (AdFi? fixed_dwell : adapt_dwell)
    document.getElementById('vlabB').style.left = `calc(${val_B/15}% + 8px)`;

    let preference = get_radio_value('preference');
    let val = (preference=='A'? val_A : val_B);
    document.getElementById('thumb').value = val;
    document.getElementById('valpref').innerHTML = parseInt(val).toFixed(0);
    document.getElementById('syspref').innerHTML = preference;
    parameters.dwell = val;
    document.getElementById('vlab').innerHTML = parseInt(val).toFixed(0);
    document.getElementById('vlab').style.left = `calc(${val/15}% + 8px)`;
    
    document.getElementById('canvas').className = 'test';

    document.getElementById('system').innerHTML = 'Training System C';
    document.getElementById('system').parentElement.className = 'test'
    parameters.set_sandbox()
    init_task();

    document.getElementById('done_thereveal').addEventListener('click', function () {
        document.getElementById('thereveal').style.display = 'none';
        document.getElementById('slider').style.display = 'none';
        document.getElementById('task').style.display = null;
        document.getElementById('canvas').className = '';
        document.getElementById('system').parentElement.className = ''

        log_slider(parameters.dwell);

        log_storyphase('xp_C');

        parameters.type = 'xp';
        parameters.storypoint = 5;
        
        document.getElementById('system').innerHTML = 'System C';
        parameters.set_xp_tasks()
        init_task();
    })
}