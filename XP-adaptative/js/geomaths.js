function inside(x, y, pt, size) {
    if (x < pt[0]) { return false; }
    if (x > pt[0] + size[0]) { return false; }
    if (y < pt[1]) { return false; }
    if (y > pt[1] + size[1]) { return false; }
    return true;
}

function inside_grab(x, y, pt, size) {
    if (x < (pt[0] + size[0]*0.8)) { return false; }
    if (x > (pt[0] + size[0]*0.9)) { return false; }
    if (y < pt[1]) { return false; }
    if (y > pt[1] + size[1]) { return false; }
    return true;
}

function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}