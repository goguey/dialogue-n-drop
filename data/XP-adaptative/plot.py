#! /usr/bin/env python3
# # -*- coding: utf-8 -*-

import sys, os, re
import getopt
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import scipy
import random
# http://seaborn.pydata.org/examples/index.html

CSV_FILE = None
RES_DIR = None

#Reading command line
def print_help():
    print('\nUsage: ')
    print('  -c [arg] or --csv [arg]: csv file to analyse (mandatory)')
    print('  -h or --help: print help (no argument)')

opts, args = getopt.getopt(sys.argv[1:], 'c:h', ["csv=", "help"])

for o, v in opts:
    if o in ('-c, --csv'):
        CSV_FILE = v
        FCMP = CSV_FILE.split('.')
        if FCMP[-1] != 'csv':
            print_help()
            sys.exit()
        RES_DIR = '{}/results'.format(os.path.dirname(CSV_FILE))

    if o in ('-h', '--help'):
        print_help()
        sys.exit()

if CSV_FILE == None:
    print_help()
    sys.exit()

if not os.path.exists(RES_DIR):
    os.makedirs(RES_DIR)
    print('Directory "{}" created.'.format(RES_DIR))
print('Directory "{}" ready to use.'.format(RES_DIR))

print('Analysing "{}"...\n'.format(CSV_FILE))
df = pd.read_csv(CSV_FILE, sep=';')

sns.set(style="whitegrid")

for t in ['Fitts Task', 'XP System A', 'XP System B', 'XP System C']:
    if not os.path.exists('{}/{}'.format(RES_DIR, t)):
        os.makedirs('{}/{}'.format(RES_DIR, t))
        print('Directory "{}/{}" created.'.format(RES_DIR, t))
        
    dff = df[df['Task Type'] == t]
    mids = dff['MID'].unique().tolist()
    random.shuffle(mids)

    for mid in mids[:20]:
        dfff = dff[dff['MID'] == mid]
        plt.clf()
        sns.scatterplot(
            data=dfff,
            x="X", y="Y", hue="Task Type", units="MID")
        plt.savefig('{}/{}/{}_{}'.format(RES_DIR, t, mid, 'trajectories_X_Y.pdf'), transparent=True)

        plt.clf()
        sns.scatterplot(
            data=dfff,
            x="Time", y="Y", hue="Task Type", units="MID")
        plt.savefig('{}/{}/{}_{}'.format(RES_DIR, t, mid, 'trajectories_Time_Y.pdf'), transparent=True)

# plt.clf()
# sns.scatterplot(
#     data=df,
#     x="X", y="Y", hue="Task Type", units="MID")
# plt.savefig('{}/{}'.format(RES_DIR, 'trajectories_X_Y.pdf'), transparent=True)

# plt.clf()
# sns.scatterplot(
#     data=df,
#     x="Time", y="Y", hue="Task Type", units="MID")
# plt.savefig('{}/{}'.format(RES_DIR, 'trajectories_Time_Y.pdf'), transparent=True)