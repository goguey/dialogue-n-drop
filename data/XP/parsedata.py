#! /usr/bin/env python3
# # -*- coding: utf-8 -*-

import os, re, sys
import getopt, json
import math

PATH  = None
FILE_OUT   = 'trial_data.csv'
DEBUG = False

#Reading command line
def print_help():
    print('\nUsage: ')
    print('  -p [arg] or --path [arg]: path to the folder containing the log folders (mandatory)')
    print('  -o [arg] or --output [arg]: name of the output csv file, will be stored in the PATH directory ("XX_trial_data.csv" by default)')
    print('  -d or --debug: print without erasing')
    print('  -h or --help: print help (no argument)')

opts, args = getopt.getopt(sys.argv[1:], 'p:o:dh', ["path=", "output=", "debug", "help"])

for o, v in opts:
    if o in ('-p, --path'):
        PATH = v
        if PATH[-1] == '/':
            PATH = PATH[0:-1]

    if o in ('-o, --output'):
        FILE_OUT = v
        FCMP = FILE_OUT.split('.')
        if FCMP[-1] != 'csv':
            FCMP.append('csv')
        FILE_OUT = '.'.join(FCMP)

    if o in ('-d', '--debug'):
        DEBUG = True

    if o in ('-h', '--help'):
        print_help()
        sys.exit()

if PATH == None:
    print_help()
    sys.exit()

##########################################
############## CSV CREATION ##############
EID = 1
def get_eid():
    global EID
    EID += 1
    return EID

MID = 1
def get_mid():
    global MID
    MID += 1
    return MID

TID = 1
def get_tid():
    global TID
    TID += 1
    return TID

def store_event(event, events, trial):
    event['EID'] = get_eid()
    event['MID'] = trial['MID']
    event['TID'] = trial['TID']

    event['User']             = trial['User']
    event['Task Type']        = trial['Task Type']
    event['Target']           = trial['Target']
    event['Dwell Value']      = trial['Dwell Value']
    event['Start Date']       = trial['Start Date']
    event['Folder Hovered']   = trial['Folder Hovered']
    event['Folder Expanded']  = trial['Folder Expanded']
    event['Try']              = trial['Try']
    event['Cursor Out']       = trial['Cursor Out']
    event['Fitts Amplitude']  = trial['Fitts Amplitude']
    event['Fitts Width']      = trial['Fitts Width']
    event['Fitts ID']         = trial['Fitts ID']
    event['Folder To Hover']  = trial['Folder To Hover']
    event['Folder To Expand'] = trial['Folder To Expand']
    event['Training']         = trial['Training']

    for key in event:
        event[key] = str(event[key])
    events.append(event)

def get_trials_info(logs, binning = 10):
    data = json.loads(logs.read())

    uid      = data['user']
    browser  = data['browser']

    events = []
    event  = None
    
    trials = []
    trial  = None

    xp = {}
    xp['User']     = str(uid)
    xp['Browser']  = str(browser.replace(';', ' ').replace(',', ' '))
    xp['Timezone'] = 'Unknown'
    if 'zone' in data:
        xp['Timezone'] = data['zone']
    
    xp['System A'] = None
    xp['System B'] = None

    cur_trial = -1
    cur_event = -1

    data = data['logs']
    data = sorted(data, key=lambda item: item['timestamp']) 
    for info in data:
        infotype  = info['type']
        timestamp = info['timestamp']
        val       = info['value']
        if DEBUG:
            print(info)

        if infotype == 'storyphase':
            xp['Phase {}'.format(val)] = timestamp
        elif infotype == 'demographics':
            xp['Age']                      = val['age']
            xp['Gender']                   = val['gender']
            xp['Computer Usage Frequency'] = val['compuse']
            xp['Game Usage Frequency']     = val['gameuse']
            xp['Pointing Device']          = val['pointing']
            pass
        elif infotype == 'newtrial':
            ########## SAFETY NET ##########
            if val['id'] <= cur_trial:
                if DEBUG:
                    # print('Uh oh... problem with the trial order #1... [{} - {}]'.format(cur_trial, val['id']))
                    print('Unfinished trial. Let\'s forget it.')
                # if not trial['Training'] and 'End Date' in trial:
                #     print(trial)
                #     print('')
                # sys.exit()
            cur_trial = val['id']
            cur_event = val['eventid']
            ################################

            trial = {}
            trial['MID'] = get_mid()
            trial['TID'] = get_tid()
            trial['User']        = str(uid)
            trial['Task Type']   = val['task']['type']
            trial['Target']      = val['task']['target']
            trial['Dwell Value'] = val['dwell']
            trial['Start Date']  = timestamp

            trial['Folder Hovered']   = 0
            trial['Folder Expanded']  = 0
            trial['Try'] = 0
            trial['Cursor Out'] = 0

            trial['Fitts Amplitude'] = -1
            trial['Fitts Width'] = -1
            trial['Fitts ID'] = -1
            
            if trial['Task Type'] == 'fitts':
                trial['Task Type'] = 'Fitts Task'
                trial['Folder To Hover']  = int(trial['Target']/100)
                trial['Folder To Expand'] = 0 
                trial['Fitts Amplitude'] = ((int(trial['Target']/100)-1)*20 + 5+5+20 - 5)
                trial['Fitts Width']     = 20
                trial['Fitts ID']        = math.log(1 + trial['Fitts Amplitude'] / trial['Fitts Width'])

                trial['Training'] = int(trial['Target']/100) % 2 == 1
            else:
                trial['Folder To Hover']  = int(trial['Target']/100) + sum([int(c) for c in str(trial['Target'] % 100)])
                trial['Folder To Expand'] = 2
                if (not 'Phase instruction_xp_A' in xp):
                    trial['Task Type'] = 'XP Training Drag-and-Drop'
                    trial['Training'] = True
                elif ('Phase instruction_xp_A' in xp) and (not 'Phase xp_A' in xp):
                    trial['Task Type'] = 'XP Training A'
                    trial['Training'] = True
                elif ('Phase xp_A' in xp) and (not 'Phase instruction_xp_B' in xp):
                    trial['Task Type'] = 'XP System A'
                    xp['System A'] = trial['Dwell Value']
                    trial['Training'] = False
                elif ('Phase instruction_xp_B' in xp) and (not 'Phase xp_B' in xp):
                    trial['Task Type'] = 'XP Training B'
                    trial['Training'] = True
                elif ('Phase xp_B' in xp) and (not 'Phase sandbox' in xp):
                    trial['Task Type'] = 'XP System B'
                    trial['Training'] = False
                    xp['System B'] = trial['Dwell Value']
                elif ('Phase sandbox' in xp) and (not 'Phase xp_C' in xp):
                    trial['Task Type'] = 'XP Training C'
                    trial['Training'] = True
                elif ('Phase xp_C' in xp) and (not 'Phase comment' in xp):
                    trial['Task Type'] = 'XP System C'
                    trial['Training'] = False
                elif 'Phase comment' in xp:
                    trial['Task Type'] = 'Post-XP'
                    trial['Training'] = True

        elif infotype == 'intrial':
            if cur_event == -1:
                if DEBUG:
                    print('Skipping intrial info cause not in a started trial....')
                continue
            ########## SAFETY NET ##########
            if val['id'] != cur_trial:
                if DEBUG:
                    print('Uh oh... problem with the trial order #2... [{} - {}]'.format(cur_trial, val['id']))
                    print('Trial: {}'.format(trial))
                # sys.exit()
                continue
            if val['eventid'] <= cur_event:
                print('Uh oh... problem with the event order #1... [{} - {}]'.format(cur_event, val['eventid']))
                print('Trial: {}'.format(trial))
                sys.exit()
            cur_event = val['eventid']
            ################################

            if val['type'] == 'box_picked':
                if not 'First Pick Date' in trial:
                    trial['First Pick Date'] = timestamp
                    trial['First Pick X'] = val['data']['x']
                    trial['First Pick Y'] = val['data']['y']
                trial['Last Pick Date'] = timestamp
                trial['Last Pick X'] = val['data']['x']
                trial['Last Pick Y'] = val['data']['y']

                trial['MID'] = get_mid()
                event = {}
                event['Type'] = 'BoxPicked'
                event['Time'] = timestamp - trial['Start Date']
                event['X'] = val['data']['x']
                event['Y'] = val['data']['y']
                event['Folder Under'] = None
                store_event(event, events, trial)

            elif val['type'] == 'box_move':
                # val['data']['x']
                # val['data']['y']
                event = {}
                event['Type'] = 'BoxMoved'
                event['Time'] = timestamp - trial['Start Date']
                event['X'] = events[-1]['X']#val['data']['x']
                event['Y'] = events[-1]['Y']#val['data']['y']
                event['Folder Under'] = None
                store_event(event, events, trial)
                pass
            elif val['type'] == 'on_nonexp_folder':
                # val['data']['folder']
                # val['data']['x']
                # val['data']['y']
                trial['Folder Hovered'] += 1

                event = {}
                event['Type'] = 'EnterItem'
                event['Time'] = timestamp - trial['Start Date']
                event['X'] = val['data']['x']
                event['Y'] = val['data']['y']
                event['Folder Under'] = val['data']['folder']
                store_event(event, events, trial)
            elif val['type'] == 'box_drop':
                # val['data']['on']
                # val['data']['x']
                # val['data']['y']
                trial['Try'] += 1

                event = {}
                event['Type'] = 'BoxDropped'
                event['Time'] = timestamp - trial['Start Date']
                event['X'] = val['data']['x']
                event['Y'] = val['data']['y']
                event['Folder Under'] = val['data']['on']
                store_event(event, events, trial)
            elif val['type'] == 'in_folder':
                # val['data']['folder']
                # val['data']['x']
                # val['data']['y']
                trial['Folder Hovered'] += 1

                event = {}
                event['Type'] = 'EnterItem'
                event['Time'] = timestamp - trial['Start Date']
                event['X'] = val['data']['x']
                event['Y'] = val['data']['y']
                event['Folder Under'] = val['data']['folder']
                store_event(event, events, trial)
            elif val['type'] == 'out_folder':
                # val['data']['folder']
                # val['data']['x']
                # val['data']['y']

                event = {}
                event['Type'] = 'LeaveItem'
                event['Time'] = timestamp - trial['Start Date']
                event['X'] = val['data']['x']
                event['Y'] = val['data']['y']
                event['Folder Under'] = val['data']['folder']
                store_event(event, events, trial)
                pass
            elif val['type'] == 'exp_folder':
                # val['data']['folder']
                trial['Folder Expanded'] += 1
                if val['data']['folder'] % 100 != 0:
                    trial['Folder Expanded'] -= 1
            elif val['type'] == 'box_cancelled':
                # val['data']['x']
                # val['data']['y']
                trial['Cursor Out'] += 1
                event = {}
                event['Type'] = 'BoxCancelled'
                event['Time'] = timestamp - trial['Start Date']
                try:
                    event['X'] = events[-1]['X']#val['data']['x']
                    event['Y'] = events[-1]['Y']#val['data']['y']
                except:
                    event['X'] = -1
                    event['Y'] = -1
                event['Folder Under'] = None
                store_event(event, events, trial)
            else:
                print(val)
                sys.exit()

        elif infotype == 'endtrial':
            if cur_event == -1:
                if DEBUG:
                    print('Skipping endtrial info cause not in a started trial....')
                continue
            ########## SAFETY NET ##########
            if val['id'] != cur_trial:
                print('Uh oh... problem with the trial order #3... [{} - {}]'.format(cur_trial, val['id']))
                print('Trial: {}'.format(trial))
                sys.exit()
            if val['eventid'] <= cur_event:
                print('Uh oh..BoxDropped. problem with the event order #2... [{} - {}]'.format(cur_event, val['eventid']))
                print('Trial: {}'.format(trial))
                sys.exit()
            cur_event = -1
            ################################

            trial['End Date']        = timestamp
            trial['Total Time']      = trial['End Date'] - trial['Start Date']
            trial['Reaction Time']   = trial['First Pick Date'] - trial['Start Date']
            trial['Full Trial Time'] = trial['End Date'] - trial['First Pick Date']
            trial['Good Trial Time'] = trial['End Date'] - trial['Last Pick Date']
            
            for key in trial:
                trial[key] = str(trial[key])
            trials.append(trial)

                
        elif infotype == 'questionnaire':
            xp['Preferred System']         = val['preference']
            xp['Preferred Time']           = xp['System A']
            if xp['Preferred System'] == 'B':
                xp['Preferred Time']           = xp['System B']
            xp['Preferred System Comment'] = val['why'].replace(';', ',').replace('\n','--')
        elif infotype == 'slider':
            xp['System C'] = val
        elif infotype == 'comment':
            xp['General Comment'] = val.replace(';', ',').replace('\n','--')
        else:
            print(infotype)
            sys.exit()

    xp['End time'] = xp['Phase end']

    xp['Phase instruction_fitts'] = xp['Phase instruction_fitts'] - xp['Phase introduction']
    xp['Phase fitts']             = xp['Phase fitts']             - xp['Phase introduction']
    xp['Phase instruction_xp']    = xp['Phase instruction_xp']    - xp['Phase introduction']
    xp['Phase instruction_xp_A']  = xp['Phase instruction_xp_A']  - xp['Phase introduction']
    xp['Phase xp_A']              = xp['Phase xp_A']              - xp['Phase introduction']
    xp['Phase instruction_xp_B']  = xp['Phase instruction_xp_B']  - xp['Phase introduction']
    xp['Phase xp_B']              = xp['Phase xp_B']              - xp['Phase introduction']
    xp['Phase questionnaire']     = xp['Phase questionnaire']     - xp['Phase introduction']
    xp['Phase comment']           = xp['Phase comment']           - xp['Phase introduction']
    xp['Phase end']               = xp['Phase end']               - xp['Phase introduction']
    xp['Phase sandbox']           = xp['Phase sandbox']           - xp['Phase introduction']
    xp['Phase xp_C']              = xp['Phase xp_C']              - xp['Phase introduction']
    xp['Phase introduction']      = xp['Phase introduction']      - xp['Phase introduction']

    for key in xp:
        xp[key] = str(xp[key])
    return events, trials, xp

##########################################
##########################################

def print_progress(progress, msg, backup):
    if backup and not DEBUG:
        sys.stdout.write("\x1b[1A\x1b[2K")
        sys.stdout.write("\x1b[1A\x1b[2K")
    print('Log crafting [\033[91m\033[1m{:30}\033[0m] {:>3}%'.format('='*math.floor(progress*30), math.floor(progress*100)))
    print(' -> {}'.format(msg))

directories = list(filter(lambda s: re.match(r'^user_', s), os.listdir(PATH)))

event_data = []
trial_data = []
xp_data = []

print_progress(0.0, 'Listing directories', False)
for i, fname in enumerate(directories, start=0):
    progress = i/len(directories)
    print_progress(progress, 'Processing {} data'.format(fname), True)

    flogs = open('{}/{}'.format(PATH, fname), 'r')
    events, trials, xp = get_trials_info(flogs)
    for event in events:
        event_data.append(event)
    for trial in trials:
        trial_data.append(trial)
    xp_data.append(xp)


for data, prefix in [ (event_data, 'events'), (trial_data, 'trials'), (xp_data, 'xp') ]:
    if len(data) > 0:
        print_progress(0.99, 'Writing csv file', True)

        fdata = open('{}/{}_{}'.format(PATH, prefix, FILE_OUT), 'w')

        # Writting CSV header
        keys = list(data[0].keys())
        fdata.write('{}\n'.format(';'.join(keys)))

        # Writting CSV data
        for d in data:
            info = [d[key] for key in keys]
            fdata.write('{}\n'.format(';'.join(info)))

        fdata.close()

print_progress(1.0, 'Done! Logs processed into "{}" .\n'.format(FILE_OUT, FILE_OUT, PATH), True)