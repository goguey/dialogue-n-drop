import pandas as pd

df_total = pd.read_csv('mturk-250-750/trials_trial_data.csv', sep=';')
df_total['Total'] = 1
df_total = df_total.groupby(['User'])
df_total = df_total.agg({'Total': 'count'}).reset_index()

df = pd.read_csv('mturk-250-750/trials_trial_data.csv', sep=';')
df['Nb'] = 1
df = df.groupby(['User', 'Task Type'])
df = df.agg({'Nb': 'count'}).reset_index()

df['Total'] = df['User'].map(df_total.set_index('User')['Total'])

print(df[df['User'] == 1236])

df = df[((df['Task Type'].isin(['XP System A', 'XP System B', 'XP System C'])) & (df['Nb'] == 12)) == False]
df = df[((df['Task Type'].isin(['Fitts Task'])) & (df['Nb'] == 20)) == False]
df = df[(df['Task Type'].isin(['XP Training A',
                               'XP Training B',
                               'XP Training C',
                               'Post-XP',
                               'XP Training Drag-and-Drop'])) == False]


print(df)

# macbook-pro-alix-main:data alixgoguey$ python verification.py 
#       User    Task Type  Nb  Total
# 271   1144  XP System C  11     98
# 309   1152   Fitts Task  18     58
# 310   1152  XP System A  10     58
# 311   1152  XP System B   8     58
# 312   1152  XP System C   7     58
# 336   1159  XP System B  11     62
# 358   1163   Fitts Task  19     76
# 434   1176  XP System A  11     77
# 675   1218   Fitts Task  19     63
# 699   1224   Fitts Task  19     63
# 708   1226  XP System A  11     66
# 773   1236   Fitts Task  15     70
# 781   1237   Fitts Task  19     70
# 782   1237  XP System A  11     70
# 988   1277  XP System A  11     71
# 989   1277  XP System B  10     71
# 1036  1289   Fitts Task  19     66
# 1120  1308   Fitts Task  19     88
# 1122  1308  XP System B  11     88
# 1130  1309  XP System B   9     65
# 1187  1316  XP System A   9     89
# 1202  1318   Fitts Task  19    159
# 1203  1318  XP System A  11    159
# 1252  1324   Fitts Task  19     58
# 1254  1324  XP System B  11     58
# 1255  1324  XP System C  10     58
# 1375  1341   Fitts Task  19     68
# 1408  1349   Fitts Task  18     61
# 1435  1358  XP System B  11     61
# 1436  1358  XP System C  11     61
# 1506  1369   Fitts Task  19     70
# 1516  1370  XP System A  11     67
# 1596  1385   Fitts Task  17     62
# 1677  1403  XP System A   7     59
# 1678  1403  XP System B  10     59
# 1710  1415   Fitts Task  19     62