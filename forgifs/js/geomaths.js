function inside(x, y, pt, size) {
    if (x < pt[0]) { return false; }
    if (x > pt[0] + size[0]) { return false; }
    if (y < pt[1]) { return false; }
    if (y > pt[1] + size[1]) { return false; }
    return true;
}

function inside_grab(x, y, pt, size) {
    if (x < (pt[0] + size[0]*0.8)) { return false; }
    if (x > (pt[0] + size[0]*0.9)) { return false; }
    if (y < pt[1]) { return false; }
    if (y > pt[1] + size[1]) { return false; }
    return true;
}