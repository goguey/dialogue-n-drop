var parameters = {};

parameters.get_label = function (level) {
    let first  = Math.floor(level / 100);
    let second = Math.floor((level - first*100) / 10);
    let third  = (level - first*100 - second*10);
    if (third == 0) {
        if (second == 0) {
            return `${first}`;
        } else {
            return `${first}.${second}`;
        }
    }
    return `${first}.${second}.${third}`;
}

parameters.dragndrop_list = function (type) {
    let h = 20;
    let w = 210;
    let ox = 5;
    let oy = ox+h+5;

    let levels = [];
    let targets = [];
    
    switch(type) {
        case 'xp':
            for (let k1 = 100; k1 <= 1600; k1 += 100) {
                levels.push(k1);
                for (let k2 = 10; k2 <= 50; k2 += 10) {
                    levels.push(k1+k2);
                    for (let k3 = 1; k3 <= 5; k3 += 1) {
                        levels.push(k1+k2+k3);
                        targets.push(k1+k2+k3);
                    }
                }
            }
            break;
        case 'fitts':
            for (let k = 100; k <= 1600; k += 100) {
                levels.push(k);
                targets.push(k);
            }
            break;
    }

    let target = targets[Math.floor(targets.length * Math.random())];
    let label = parameters.get_label(target);
    let box = { 'id': 1, 'level': 1, 'opos': [ox, ox], 'pos': [ox, ox], 'size': [w, h], 'label': `drop in ${label}`, 'target': target };

    let items = [];
    for (let k = 0; k < levels.length; k++) {
        let label = parameters.get_label(levels[k]);
        let depth = label.split('.');
        let item = {
            'id': levels[k], 'exp': true, 'hierachy': depth.length, 'goto':2,
            'opos': [ox, (parseInt(depth[0])-1)*h+oy], 'pos': [ox, (parseInt(depth[0])-1)*h+oy],
            'size': [w, h], 'label': label, 'sublist': []
        };
        switch (depth.length) {
            case 1:
                items.push(item);
                break;
            case 2:
                items[(parseInt(depth[0])-1)].sublist.push(item);
                items[(parseInt(depth[0])-1)].exp = false;
                break;
            case 3:
                items[(parseInt(depth[0])-1)].sublist[(parseInt(depth[1])-1)].sublist.push(item);
                items[(parseInt(depth[0])-1)].sublist[(parseInt(depth[1])-1)].exp = false;
        }
    }

    return { 'target' : box, 'list' : items }
}

parameters.get_task = function(type) {
    return {
        'type': type,
        'value': parameters.dragndrop_list(type)
    }
}

var colors = {
    butlabel: '#FFFFFF',
    button:   '#2C66D2',

    inzone:       '#EEEEEE',
    inzoneblink:  '#5CA7EA',
    outzone:      '#7D7D7D',
    outzoneblink: '#3D3D9D',

    label: '#000000',
    boxlabel: '#FFFFFF',
    
    inbox:     '#2154AE',
    outbox:    '#242424',
    activeinbox: '#2154AEBB',
    activeinrightbox: '#68DC72BB',
    activeoutbox: '#242424BB',

    done: '#02881f',
    background: 'white'
}