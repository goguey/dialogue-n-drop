var cursor = { 'ok': false };
var ontask = false;
var ontrial = false;

var action = 'nothing';
var level = 0;
var box_id = null;
var hovered_item = null;

function init_task() {
    ontask = true; ontrial = false;
    clear();

    level = 0;
    button('Start', 'center', init_trial);
}

function init_trial() {
    if (!(ontask && ! ontrial)) { return; }
    ontask = false; ontrial = true;
    clear();

    // let task = parameters.get_task('fitts');
    let task = parameters.get_task('xp');

    for (let k = 0; k < task.value.list.length; k++) {
        list(task.value.list[k]);
    }
    box(task.value.target);

    level = 1;
    update_scene();
}

function trial_done() {
    // clear();
    canvas.style.backgroundColor = colors.done;
    setTimeout(function (){
        canvas.style.backgroundColor = colors.background;
        init_task();
        init_trial();
    }, 200);
}

function interact() {
    if (action == 'nothing') {
        box_id = get_box(cursor.x, cursor.y);
        if (box_id != null) {
            action = 'moving box';
        }
    }

    if (action == 'moving box') {
        move_box(box_id, cursor.x - cursor.oldx, cursor.y - cursor.oldy);
        hover_list(box_id, cursor.x, cursor.y);
    }

    update_scene(level, box_id);
}

function down(ev) {
    cursor = {
        'ok': true,
        'type': 'down',
        'x': ev.clientX - canvas.offsetLeft,
        'y': ev.clientY - canvas.offsetTop,
        'oldx': ev.clientX - canvas.offsetLeft,
        'oldy': ev.clientY - canvas.offsetTop
    };
    interact();
}

function move(ev) {
    if (cursor['ok'] && ev.buttons == 1) {
        cursor = {
            'ok': true,
            'type': 'move',
            'x': ev.clientX - canvas.offsetLeft,
            'y': ev.clientY - canvas.offsetTop,
            'oldx': cursor.x,
            'oldy': cursor.y
        };
        interact();
    } else {
        up(ev);
    }
}

function move_out(ev) {
    if (cursor['ok'] && ev.buttons == 1) {
        let x = ev.clientX - canvas.offsetLeft;
        let y = ev.clientY - canvas.offsetTop;
        if (x < 0 || x > canvas.clientWidth ||
            y < 0 || y > canvas.clientHeight) {
                // if cursor out -> cancel trial
                up(ev);
        }
    }
}

function up(ev) {
    if (cursor['ok']) {
        if (action == 'moving box') {
            drop_box(box_id, level, ev.clientX - canvas.offsetLeft, ev.clientY - canvas.offsetTop);
        }
        action = 'nothing';
        box_id = null;
    }
    cursor = { 'ok': false };

    update_scene(level, box_id);
}

function init(){
    canvas = document.getElementById('canvas');
    paper.setup(canvas);

    parameters.dwell = 500;
    init_task();

    //Listeners
    canvas.addEventListener('mousedown', function(e)  { down(e) });
    canvas.addEventListener('mousemove', function(e)  { move(e); });
    window.addEventListener('mousemove', function(e)  { move_out(e); });
    canvas.addEventListener('mouseup', function(e)    { up(e); });

    document.getElementById('thumb').addEventListener('input', function(e)  {
        let val = document.getElementById('thumb').value
        document.getElementById('vlab').innerHTML = val;
        document.getElementById('vlab').style.left = `calc(${val/15}% + 8px)`;

        parameters.dwell = val;
    });
}