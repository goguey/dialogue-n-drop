var at_least_once = false;
var slider_changed = false;

var cursor = { 'ok': false };
var ontask = false;
var ontrial = false;

var action = 'nothing';
var level = 0;
var box_id = null;
var hovered_item = null;

var cur_task = null;
var in_sandbox = false;

function init_task() {
    console.log('init_task');
    ontask = true; ontrial = false;
    clear();

    action = 'nothing';
    level = 0;
    box_id = null;
    hovered_item = null;
    // button('Start', 'center', init_trial);
    init_trial();
}

function init_trial() {
    console.log('init_trial');
    if (!(ontask && ! ontrial)) { return; }
    ontask = false; ontrial = true;
    clear();

    cur_task = parameters.get_task();
    if (parameters._dwell === undefined && (
        ((current_phase == 'xp_A') &&  AdFi) ||
        ((current_phase == 'xp_B') && !AdFi)
    )) {
        adapt_dwells.push(parameters.dwell);
        console.log(`adapt_dwells added!`);
    }

    if (cur_task == null) { task_done(); return; }
    log_new_trial(cur_task);

    for (let k = 0; k < cur_task.value.list.length; k++) {
        list(cur_task.value.list[k]);
    }
    box(cur_task.value.target);

    level = 1;
    update_scene();
}

function trial_done() {
    log_trial_done(cur_task);
    
    // if (ontrial && current_phase == 'instruction_xp') {
    //     parameters.compute_last_mct();
    // }
    if (ontrial && (
            (current_phase == 'instruction_xp') ||
            ((current_phase == 'xp_A') &&  AdFi) ||
            ((current_phase == 'xp_B') && !AdFi)
        )) {
        parameters.compute_last_mct();
    }
    ontrial = false;

    // clear();
    canvas.style.backgroundColor = colors.done;
    hide_box(box_id);
    setTimeout(function (){
        canvas.style.backgroundColor = colors.background;
        init_task();
        // init_trial();
    }, 500);

    at_least_once = true;
    if (slider_changed) {
        document.getElementById('done_thereveal').style.display = null;
    }
    document.getElementById('done_instruction_xp_A').style.display = null;
    document.getElementById('done_instruction_xp_B').style.display = null;
}

function interact() {
    if (action == 'nothing') {
        box_id = get_box(cursor.x, cursor.y);
        if (box_id != null) {
            action = 'moving box';
            log_in_trial('box_picked', {'x': cursor.x, 'y': cursor.y});
        }
    }

    if (action == 'moving box') {
        log_in_trial('box_move', {'x': cursor.x, 'y': cursor.y});
        // move_box(box_id, cursor.x - cursor.oldx, cursor.y - cursor.oldy);
        move_box(box_id, cursor.x, cursor.y);
        hover_list(box_id, cursor.x, cursor.y);
    }

    update_scene(level, box_id);
}

function down(ev) {
    cursor = {
        'ok': true,
        'type': 'down',
        'x': ev.clientX - canvas.offsetLeft,
        'y': ev.clientY - canvas.offsetTop,
        'oldx': ev.clientX - canvas.offsetLeft,
        'oldy': ev.clientY - canvas.offsetTop
    };
    interact();
}

function move(ev) {
    if (cursor['ok'] && ev.buttons == 1) {
        cursor = {
            'ok': true,
            'type': 'move',
            'x': ev.clientX - canvas.offsetLeft,
            'y': ev.clientY - canvas.offsetTop,
            'oldx': cursor.x,
            'oldy': cursor.y
        };
        interact();
    } else {
        up(ev);
    }
}

function move_out(ev) {
    if (cursor['ok'] && ev.buttons == 1) {
        let x = ev.clientX - canvas.offsetLeft;
        let y = ev.clientY - canvas.offsetTop;
        if (x < 0 || x > canvas.clientWidth ||
            y < 0 || y > canvas.clientHeight) {
                // if cursor out -> cancel trial
                log_in_trial('box_cancelled', {'x': x, 'y': y});
                up(ev);
        }
    }
}

function up(ev) {
    if (cursor['ok']) {
        if (action == 'moving box') {
            log_in_trial('box_drop', {'x': ev.clientX - canvas.offsetLeft, 'y': ev.clientY - canvas.offsetTop, 'on': hovered_item});
            drop_box(box_id, level, ev.clientX - canvas.offsetLeft, ev.clientY - canvas.offsetTop);
        }
        action = 'nothing';
        box_id = null;
    }
    cursor = { 'ok': false };

    update_scene(level, box_id);
}

function init(){
    canvas = document.getElementById('canvas');
    paper.setup(canvas);

    document.getElementById("contract").addEventListener('input', function(e) {
        let curtext = document.getElementById("contract").value;
        let targettext = document.getElementById("citation").innerHTML;
        curtext = curtext.toLowerCase();
        targettext = targettext.toLowerCase();
        curtext = curtext.replaceAll('.','').replaceAll(',','');
        targettext = targettext.replaceAll('.','').replaceAll(',','');
        if (DEBUG || (curtext == targettext)) {
            document.getElementById("check").className = "check-ok";
            document.getElementById("check").src = 'imgs/check.png'
            document.getElementById('done_instruction_xp_A_B').style.display = null;
        } else {
            document.getElementById("check").className = "check-ko";
            document.getElementById("check").src = 'imgs/forbidden.png'
        }
    });
    document.getElementById('done_instruction_xp_A_B').addEventListener('click', function(e) {
        document.getElementById('instruction_xp_A_B').style.display = "none";
        document.getElementById('instruction_xp_A').style.display = null;
        document.getElementById('task').style.display = null;
    });

    parameters.dwell = 500;
    // init_task();

    document.getElementById('nbtrial-instructions').innerHTML = `${training_nb_trials}`;

    //Listeners
    canvas.addEventListener('mousedown', function(e)  { down(e) });
    canvas.addEventListener('mousemove', function(e)  { move(e); });
    window.addEventListener('mousemove', function(e)  { move_out(e); });
    canvas.addEventListener('mouseup', function(e)    { up(e); });

    document.getElementById('thumb').addEventListener('input', function(e)  {
        slider_changed = true;
        at_least_once = false;

        let val = document.getElementById('thumb').value
        document.getElementById('vlab').innerHTML = parseInt(val).toFixed(0);
        document.getElementById('vlab').style.left = `calc(${val/15}% + 8px)`;

        parameters.dwell = val;

        if (at_least_once) {
            document.getElementById('done_thereveal').style.display = null;
        }
    });

    init_log();
    init_story();
    resize();
    check_smartphone();
}

function resize() {
    if (window.innerWidth < 900 || window.innerHeight < 700) {
        document.getElementById('wrongsize').style.display = 'inherit';
    } else {
        document.getElementById('wrongsize').style.display = 'none';
    }
    check_smartphone();
}

function check_smartphone() {
    if (navigator.userAgent.match(/Android/i) ||
        navigator.userAgent.match(/webOS/i)   ||
        navigator.userAgent.match(/iPhone/i)  ||
        navigator.userAgent.match(/iPad/i)    ||
        navigator.userAgent.match(/iPod/i)    ||
        navigator.userAgent.match(/BlackBerry/i)) {
        document.getElementById('wrongsmart').style.display = 'inherit';
        document.getElementById('wrongsize').style.display = 'none';
    } else {
        document.getElementById('wrongsmart').style.display = 'none';
    }
}