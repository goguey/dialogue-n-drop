var cursor = { 'ok': false };
var ontask = false;
var ontrial = false;

var action = 'nothing';
var level = 0;
var box_id = null;

function init_task() {
    ontask = true; ontrial = false;
    clear();

    level = 0;
    button('Start Trial', 'center', init_trial);
}

function init_trial() {
    if (!(ontask && ! ontrial)) { return; }
    ontask = false; ontrial = true;
    clear();

    let task = parameters.get_task();

    // if (task.type == 'boxes') {
    //     for (let k = 0; k < task.value.zones.length; k++) {
    //         zone(task.value.zones[k]);
    //     }
    //     for (let k = 0; k < task.value.boxes.length; k++) {
    //         box(task.value.boxes[k]);
    //     }
    // }
    if (task.type == 'list') {
        for (let k = 0; k < task.value.list.length; k++) {
            list(task.value.list[k]);
        }
        for (let k = 0; k < task.value.boxes.length; k++) {
            box(task.value.boxes[k]);
        }
    }

    level = 1;
    update_scene();
}

function interact() {
    if (action == 'nothing') {
        box_id = get_box(cursor.x, cursor.y);
        if (box_id != null) {
            action = 'moving box';
        }
    }

    if (action == 'moving box') {
        move_box(box_id, cursor.x - cursor.oldx, cursor.y - cursor.oldy);
        // hover_box(box_id, level, cursor.x, cursor.y);
        hover_list(box_id, cursor.x, cursor.y);
    }

    update_scene(level, box_id);
}

function down(ev) {
    cursor = {
        'ok': true,
        'type': 'down',
        'x': ev.clientX - canvas.offsetLeft,
        'y': ev.clientY - canvas.offsetTop,
        'oldx': ev.clientX - canvas.offsetLeft,
        'oldy': ev.clientY - canvas.offsetTop
    };
    interact();
}

function move(ev) {
    if (cursor['ok'] && ev.buttons == 1) {
        cursor = {
            'ok': true,
            'type': 'move',
            'x': ev.clientX - canvas.offsetLeft,
            'y': ev.clientY - canvas.offsetTop,
            'oldx': cursor.x,
            'oldy': cursor.y
        };
        interact();
    } else {
        up(ev);
    }
}

function up(ev) {
    if (cursor['ok']) {
        if (action == 'moving box') {
            drop_box(box_id, level, ev.clientX - canvas.offsetLeft, ev.clientY - canvas.offsetTop);
            // no_zone();
        }
        action = 'nothing';
        box_id = null;
    }
    cursor = { 'ok': false };

    update_scene(level, box_id);
}

function init(){
    canvas = document.getElementById('canvas');
    paper.setup(canvas);

    init_task();

    //Listeners
    canvas.addEventListener('mousedown', function(e)  { down(e) });
    canvas.addEventListener('mousemove', function(e)  { move(e); });
    canvas.addEventListener('mouseup', function(e)    { up(e); });
}