var canvas = null;
var scene = [];
var boxes = [];
// var zones = [];
var items = [];
var labels = [];

var zone_opening_info = null;
var item_expanding_info = null;

function clear() {
    for (let k = 0; k < scene.length; k++) {
        scene[k].remove()
    }
    scene = [];

    for (let k = 0; k < boxes.length; k++) {
        boxes[k].remove()
    }
    boxes = [];

    // for (let k = 0; k < zones.length; k++) {
    //     zones[k].remove()
    // }
    // zones = [];

    for (let k = 0; k < items.length; k++) {
        items[k].remove()
    }
    items = [];

    for (let k = 0; k < labels.length; k++) {
        labels[k].remove()
    }
    labels = [];

    paper.view.update();
}

function update_scene() {
    for (let k = 0; k < boxes.length; k++) {
        if (boxes[k].params.level == level) {
            boxes[k].opacity = 1.0;
        } else {
            boxes[k].opacity = 0.0;
        }

        if (boxes[k].params.id == box_id) {
            boxes[k].strokeColor = colors.activebox;
        } else {
            boxes[k].strokeColor = colors.outbox;
        }
    }

    // for (let k = 0; k < zones.length; k++) {
    //     if (zones[k].params.level == level) {
    //         zones[k].opacity = 1.0;
    //     } else {
    //         zones[k].opacity = 0.0;
    //     }
    // }
}

function get_box(x, y) {
    for (let k = boxes.length - 1; k >= 0; k--) {
        if (inside(x, y, boxes[k].params.point, boxes[k].params.size)) {
            return boxes[k].params.id;
        }
    }
    return null;
}

function move_box(id, dx, dy) {
    for (let k = 0; k < boxes.length; k++) {
        if (boxes[k].params.id == id) {
            boxes[k].params.point[0] += dx;
            boxes[k].params.point[1] += dy;
            boxes[k].translate([dx,dy]);
            break;
        }
    }
}

// function hover_box(id, level, x, y) {
//     let zone = null;
//     for (let k = 0; k < zones.length; k++) {
//         if (zone == null &&
//             zones[k].params.level == level &&
//             inside(x, y, zones[k].params.point, zones[k].params.size)) {
//             zone = k;
//             break;
//         }
//     }

//     if (zone != null) {
//         open_zone(zone, id);
//     } else {
//         no_zone(zone);
//     }
// }

function hover_list(id, x, y) {
    let item = null;
    // for (let k = items.length - 1; k >= 0; k--) {
    for (let k = 0; k < items.length; k++) {
        if (item == null &&
            inside(x, y, items[k].params.point, items[k].params.size)) {
            item = k;
            console.log(items[k].params.label)
            break;
        }
    }

    if (item != null) {
        expand_item(item, id);
    } else {
        // no_item(zone);
    }
}

// function open_zone(zone_id, box_id) {
//     if (zone_opening_info == null) {
//         zone_opening_info = {};
//         zone_opening_info.k = zone_id;
//         zone_opening_info.id = zones[zone_id].params.id;
//         zone_opening_info.box = box_id;
//         zone_opening_info.handle = setTimeout( function() {
//             clearInterval(zone_opening_info.blink);
//             clearTimeout(zone_opening_info.handle);
//             open_level(zone_id, box_id);
//             zone_opening_info = null;
//         }, zones[zone_id].params.dwell);
//         zone_opening_info.active = false;
//         zone_opening_info.blink = setInterval( function() {
//             if (zone_opening_info.active) {
//                 zones[zone_id].fillColor = colors.inzoneblink;
//                 zones[zone_id].strokeColor = colors.outzoneblink;
//             } else {
//                 zones[zone_id].fillColor = colors.inzone;
//                 zones[zone_id].strokeColor = colors.outzone;
//             }
//             zone_opening_info.active = !zone_opening_info.active;
//             update_scene();
//         }, Math.min(100, zones[zone_id].params.dwell * 0.2));
//     } else {
//         if (zone_opening_info.id != zones[zone_id].params.id) { no_zone(); }
//     }
// }

// function open_level(zone_id, box_id) {
//     level = zones[zone_id].params.goto;
//     for (let k = 0; k < boxes.length; k++) {
//         if (boxes[k].params.id == box_id) {
//             boxes[k].params.level = level;
//             break;
//         }
//     }

//     update_scene()
// }

// function no_zone() {
//     if (zone_opening_info != null) {
//         zones[zone_opening_info.k].fillColor = colors.inzone;
//         zones[zone_opening_info.k].strokeColor = colors.outzone;
//         clearInterval(zone_opening_info.blink);
//         clearTimeout(zone_opening_info.handle);
//         zone_opening_info = null;
//     }
// }

function expand_item(item_id, box_id) {
    if (item_expanding_info == null) {
        item_expanding_info = {};
        item_expanding_info.k = item_id;
        item_expanding_info.id = items[item_id].params.id;
        item_expanding_info.box = box_id;
        item_expanding_info.handle = setTimeout( function() {
            clearInterval(item_expanding_info.blink);
            clearTimeout(item_expanding_info.handle);
            if (!items[item_id].params.expanded) { expanding(item_id); }
            item_expanding_info = null;
        }, items[item_id].params.dwell);
        item_expanding_info.active = false;
        item_expanding_info.blink = setInterval( function() {
            if (item_expanding_info.active) {
                items[item_id].fillColor = colors.inzoneblink;
                items[item_id].strokeColor = colors.outzoneblink;
            } else {
                items[item_id].fillColor = colors.inzone;
                items[item_id].strokeColor = colors.outzone;
            }
            item_expanding_info.active = !item_expanding_info.active;
            update_scene();
        }, Math.min(100, items[item_id].params.dwell * 0.2));
    } else {
        if (item_expanding_info.id != items[item_id].params.id) { no_item(); }
    }
}

function expanding(item_id) {
    let offset = 0;
    let unfolding = false;
    for (let k = 0; k < items.length; k++) {
        if (unfolding) {
            if (items[k].params.hierachy == items[item_id].params.hierachy+1) {
                offset += items[k].params.size[1];
            } else if (items[k].params.hierachy == items[item_id].params.hierachy) {
                unfolding = false;
            }
        }

        items[k].params.point[1] += offset;
        items[k].translate([0,offset]);
        labels[k].translate([0,offset]);

        if (items[k].params.id == items[item_id].params.id) {
            unfolding = true;
            items[k].params.expanded = true;
            labels[k].content = labels[k].content.replace('▶', '▼')
        }
    }

    update_scene()
}

function no_item() {
    if (item_expanding_info != null) {
        items[item_expanding_info.k].fillColor = colors.inzone;
        items[item_expanding_info.k].strokeColor = colors.outzone;
        clearInterval(item_expanding_info.blink);
        clearTimeout(item_expanding_info.handle);
        item_expanding_info = null;
    }
}

function drop_box(id, level, x, y) {
    // for (let k = 0; k < zones.length; k++) {
    //     if (zones[k].params.level == level &&
    //         inside(x, y, zones[k].params.point, zones[k].params.size)) {

    //         for (let l = 0; l < boxes.length; l++) {
    //             if (boxes[l].params.id == id) {
    //                 boxes[l].params.level = zones[k].params.goto;
    //                 no_zone();
    //                 return;
    //             }
    //         }
    //     }
    // }

    for (let k = 0; k < items.length; k++) {
        if (inside(x, y, items[k].params.point, items[k].params.size)) {

            for (let l = 0; l < boxes.length; l++) {
                if (boxes[l].params.id == id) {
                    boxes[l].params.level = items[k].params.goto;
                    no_item();
                    return;
                }
            }
        }
    }
}

function box(params) {
    let out = 6;
    let rect =  new paper.Path.Rectangle({
        point: [params.point[0]+out*0.5, params.point[1]+out*0.5],
        size: [params.size[0]-out, params.size[1]-out],
        fillColor: colors.inbox,
        strokeColor: colors.outbox,
        strokeWidth: out,
        params: params
    });

    boxes.push(rect);
}

// function zone(params) {
//     let out = 5;
//     let rect =  new paper.Path.Rectangle({
//         point: [params.point[0]+out*0.5, params.point[1]+out*0.5],
//         size: [params.size[0]-out, params.size[1]-out],
//         fillColor: colors.inzone,
//         strokeColor: colors.outzone,
//         strokeWidth: out,
//         params: params
//     });
//     zones.push(rect);
// }

function string_hierachy(hierachy, expanded) {
    let end = '  ';
    if (!expanded) { end = '▶'+end; } else { end += ' '; }
    switch(hierachy) {
        case 1:
            return end;
        case 2:
            return '       │'+end;
        case 3:
            return '       │       │'+end;
        case 4:
            return '       │       │       │'+end;
        default:
            return '';
    }
}

function list(params, top=true) {
    let _items = [];
    let _labels = [];

    for (let k = 0; k < params.sublist.length; k++) {
        let res = list(params.sublist[k], false);
        _items  = _items.concat(res[0]);
        _labels = _labels.concat(res[1]);
    }

    let out = 1;
    let rect =  new paper.Path.Rectangle({
        point: [params.point[0]+out*0.5, params.point[1]+out*0.5],
        size: [params.size[0]-out, params.size[1]-out],
        fillColor: colors.inzone,
        strokeColor: colors.outzone,
        strokeWidth: out,
        params: params
    });
    let label = new paper.PointText({
        point: [params.point[0] + 10, params.point[1] + params.size[1]*0.5 + 5],
        fillColor: colors.label,
        fontSize: '15px',
        justification: 'left',
        content: string_hierachy(params.hierachy, params.expanded)+params.label
    });

    _items = [rect].concat(_items);
    _labels = [label].concat(_labels);

    if (!top) {
        return [_items, _labels];
    } else {
        items = items.concat(_items);
        labels = labels.concat(_labels);
    }
}

function _button(text, x, y , w, h) {
    let rect =  new paper.Path.Rectangle({
        point: [x-1, y-1],
        size: [w, h],
        strokeColor: 'black',
        strokeWidth: 2,
        fillColor: colors.button
    });

    let label = new paper.PointText({
        point: [x + w*0.5, y + h*0.5 + 5],
        fillColor: colors.butlabel,
        fontSize: '20px',
        justification: 'center',
        content: text
    });

    return [rect, label];
}

function button(text, pos, callback) {
    let res = null
    let w = paper.view.size.getWidth();
    let h = paper.view.size.getHeight();

    if (pos == 'center') {
        res = _button(text, w * 0.25, h * 0.45, w * 0.5, h * 0.1);
    } else {
        res = _button(text, w * 0.25, h * 0.25, w * 0.5, h * 0.5);
    }
    let but = res[0];
    but.onClick = callback;
    scene.push(but);

    let label = res[1];
    label.onClick = callback;
    scene.push(label);
}