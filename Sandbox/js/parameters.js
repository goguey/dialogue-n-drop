var parameters = {};

// parameters.dragndrop_boxes = function () {
//     return {
//         'boxes' : [
//             { 'id': 1, 'level': 1, 'point': [ 50,50], 'size': [90,90] },
//             { 'id': 2, 'level': 1, 'point': [200,50], 'size': [90,90] },
//             { 'id': 3, 'level': 1, 'point': [350,50], 'size': [90,90] }
//         ],
//         'zones' : [
//             { 'id': 1, 'level': 1, 'goto':2, 'dwell': 1000, 'point': [225,550], 'size': [100,100] },
//             { 'id': 2, 'level': 2, 'goto':3, 'dwell': 2500, 'point': [375,550], 'size': [100,100] }
//         ],
//     }
// }

parameters.dragndrop_list = function () {
    return {
        'boxes' : [
            { 'id': 1, 'level': 1, 'point': [350, 20], 'size': [30,30] },
            { 'id': 2, 'level': 1, 'point': [350, 60], 'size': [30,30] },
            { 'id': 3, 'level': 1, 'point': [350,100], 'size': [30,30] }
        ],
        'list' : [
            { 'id': 100, 'expanded': false, 'hierachy': 1, 'goto':2, 'dwell': 1000, 'point': [0,  0], 'size': [300,25], 'label': 'Folder 100', 'sublist': [
                { 'id': 110, 'expanded': false, 'hierachy': 2, 'goto':2, 'dwell': 1000, 'point': [0,  0], 'size': [300,25], 'label': 'Folder 110', 'sublist': [
                    { 'id': 111, 'expanded': true, 'hierachy': 3, 'goto':2, 'dwell': 1000, 'point': [0,  0], 'size': [300,25], 'label': 'Folder 111', 'sublist': [] },
                    { 'id': 112, 'expanded': true, 'hierachy': 3, 'goto':2, 'dwell': 1000, 'point': [0,  0], 'size': [300,25], 'label': 'Folder 112', 'sublist': [] },
                    { 'id': 113, 'expanded': true, 'hierachy': 3, 'goto':2, 'dwell': 1000, 'point': [0,  0], 'size': [300,25], 'label': 'Folder 113', 'sublist': [] },
                ] },
                { 'id': 120, 'expanded': true, 'hierachy': 2, 'goto':2, 'dwell': 1000, 'point': [0,  0], 'size': [300,25], 'label': 'Folder 120', 'sublist': [] },
                { 'id': 130, 'expanded': true, 'hierachy': 2, 'goto':2, 'dwell': 1000, 'point': [0,  0], 'size': [300,25], 'label': 'Folder 130', 'sublist': [] }
            ] },
            { 'id': 200, 'expanded': true, 'hierachy': 1, 'goto':2, 'dwell': 1000, 'point': [0, 25], 'size': [300,25], 'label': 'Folder 200', 'sublist': [] },
            { 'id': 300, 'expanded': true, 'hierachy': 1, 'goto':2, 'dwell': 1000, 'point': [0, 50], 'size': [300,25], 'label': 'Folder 300', 'sublist': [] },
            { 'id': 400, 'expanded': true, 'hierachy': 1, 'goto':2, 'dwell': 1000, 'point': [0, 75], 'size': [300,25], 'label': 'Folder 400', 'sublist': [] },
            { 'id': 500, 'expanded': true, 'hierachy': 1, 'goto':2, 'dwell': 1000, 'point': [0,100], 'size': [300,25], 'label': 'Folder 500', 'sublist': [] },
            { 'id': 600, 'expanded': true, 'hierachy': 1, 'goto':2, 'dwell': 1000, 'point': [0,125], 'size': [300,25], 'label': 'Folder 600', 'sublist': [] },
            { 'id': 700, 'expanded': true, 'hierachy': 1, 'goto':2, 'dwell': 1000, 'point': [0,150], 'size': [300,25], 'label': 'Folder 700', 'sublist': [] },
            { 'id': 800, 'expanded': true, 'hierachy': 1, 'goto':2, 'dwell': 1000, 'point': [0,175], 'size': [300,25], 'label': 'Folder 800', 'sublist': [] }
        ],
    }
}

parameters.get_task = function() {
    // if (Math.round(Math.random()) == 0) {
    //     return {
    //         'type': 'boxes',
    //         'value': parameters.dragndrop_boxes()
    //     }
    // } else {
        return {
            'type': 'list',
            'value': parameters.dragndrop_list()
        }
    // }
}

var colors = {
    butlabel: '#FFFFFF',
    button:   '#2C66D2',

    inzone:       '#EEEEEE',
    inzoneblink:  '#5E5EEE',
    outzone:      '#7D7D7D',
    outzoneblink: '#3D3D9D',

    label: '#000000',
    
    outbox:    '#65676B',
    inbox:     '#242424',
    activebox: '#2C66D2',
}