var canvas = null;
var scene = [];
var boxes = [];
var items = [];
var labels = [];
var hidden = false;

var zone_opening_info = null;
var item_expanding_info = null;

function clear() {
    hidden = false;

    for (let k = 0; k < scene.length; k++) {
        scene[k].remove()
    }
    scene = [];

    for (let k = 0; k < boxes.length; k++) {
        boxes[k].box.remove()
        boxes[k].label.remove()
        boxes[k].ghost.remove()
        boxes[k].ghost_label.remove()
        boxes[k].grab1.remove()
        boxes[k].grab2.remove()
        boxes[k].grab3.remove()
        boxes[k].grab4.remove()
    }
    boxes = [];

    for (let k = 0; k < items.length; k++) {
        items[k].remove()
    }
    items = [];

    for (let k = 0; k < labels.length; k++) {
        labels[k].remove()
    }
    labels = [];

    paper.view.update();
}

function update_scene() {
    for (let k = 0; k < boxes.length; k++) {
        if (boxes[k].box.params.level == level) {
            boxes[k].box.opacity = 1.0;
            if (boxes[k].label != null) { boxes[k].label.opacity = 1.0; }
        } else {
            boxes[k].box.opacity = 0.0;
            if (boxes[k].label != null) { boxes[k].label.opacity = 0.0; }
        }

        if (boxes[k].box.params.id == box_id) {
            boxes[k].box.strokeColor = colors.activeoutbox;
            boxes[k].box.fillColor = colors.activeinbox;
            boxes[k].grab1.opacity = 0.0;
            boxes[k].grab2.opacity = 0.0;
            boxes[k].grab3.opacity = 0.0;
            boxes[k].grab4.opacity = 0.0;
            if (boxes[k].box.params.target == hovered_item) {
                boxes[k].box.fillColor = colors.activeinrightbox;
            }
        } else {
            boxes[k].box.strokeColor = colors.outbox;
            boxes[k].box.fillColor = colors.inbox;
            if (!hidden) {
                boxes[k].grab1.opacity = 1.0;
                boxes[k].grab2.opacity = 1.0;
                boxes[k].grab3.opacity = 1.0;
                boxes[k].grab4.opacity = 1.0;
            }
        }
    }
}

function hide_box(id) {
    for (let k = 0; k < boxes.length; k++) {
        if (boxes[k].box.params.id == id) {
            hidden = true;
            boxes[k].box.opacity = 0.0;
            boxes[k].grab1.opacity = 0.0;
            boxes[k].grab2.opacity = 0.0;
            boxes[k].grab3.opacity = 0.0;
            boxes[k].grab4.opacity = 0.0;
            boxes[k].label.opacity = 0.0;
            boxes[k].ghost.opacity = 0.0;
            boxes[k].ghost_label.opacity = 0.0;
            break;
        }
    }
}

function get_box(x, y) {
    for (let k = boxes.length - 1; k >= 0; k--) {
        // if (inside(x, y, boxes[k].box.params.pos, boxes[k].box.params.size)) {
        if (inside_grab(x, y, boxes[k].box.params.pos, boxes[k].box.params.size)) {
            boxes[k].box.params.dx = boxes[k].box.params.pos[0] - x;
            boxes[k].box.params.dy = boxes[k].box.params.pos[1] - y;
            return boxes[k].box.params.id;
        }
    }
    return null;
}

// function move_box(id, dx, dy) {
function move_box(id, cx, cy) {
    for (let k = 0; k < boxes.length; k++) {
        if (boxes[k].box.params.id == id) {
            // boxes[k].box.params.pos[0] += dx;
            // boxes[k].box.params.pos[1] += dy;
            
            // absolute variant
            let dx = boxes[k].box.params.pos[0];
            let dy = boxes[k].box.params.pos[1];
            boxes[k].box.params.pos[0] = cx + boxes[k].box.params.dx;
            boxes[k].box.params.pos[1] = cy + boxes[k].box.params.dy;
            dx = boxes[k].box.params.pos[0] - dx;
            dy = boxes[k].box.params.pos[1] - dy;
            ////////////////////
            boxes[k].box.translate([dx,dy]);
            if (boxes[k].label != null) { boxes[k].label.translate([dx,dy]); }
            break;
        }
    }
}

function hover_list(id, x, y) {
    let item = null;
    for (let k = 0; k < items.length; k++) {
        if (item == null &&
            inside(x, y, items[k].params.pos, items[k].params.size)) {
            item = k;
            hovered_item = items[k].params.id;
            break;
        }
    }

    if (item != null) {
        expand_item(item, id, x, y);
    } else {
        no_item(x, y);
        hovered_item = null;
        last_non_exp_folder = null;
    }
}

var last_non_exp_folder = null;
function expand_item(item_id, box_id, x, y) {
    if (item_expanding_info == null) {
        no_item(x, y);
        if (items[item_id].params.sublist.length && !items[item_id].params.exp) {
            log_in_trial('in_folder', {'folder': items[item_id].params.id, 'x': x, 'y': y});
            item_expanding_info = {};
            item_expanding_info.k = item_id;
            item_expanding_info.id = items[item_id].params.id;
            item_expanding_info.box = box_id;
            item_expanding_info.handle = setTimeout( function() {
                clearInterval(item_expanding_info.blink);
                clearTimeout(item_expanding_info.handle);
                if (!items[item_id].params.exp) { collapse_all(); expanding(item_id); }
                item_expanding_info = null;
                items[item_id].fillColor = colors.inzoneblink;
                items[item_id].strokeColor = colors.outzoneblink;
            }, parameters.dwell);
            
            item_expanding_info.active = true;
            items[item_id].fillColor = colors.inzone;
            items[item_id].strokeColor = colors.outzone;
            item_expanding_info.blink = setInterval( function() {
                if (item_expanding_info.active) {
                    items[item_id].fillColor = colors.inzoneblink;
                    items[item_id].strokeColor = colors.outzoneblink;
                } else {
                    items[item_id].fillColor = colors.inzone;
                    items[item_id].strokeColor = colors.outzone;
                }
                item_expanding_info.active = !item_expanding_info.active;
                update_scene();
            }, 150);//Math.min(50, parameters.dwell * 0.2));
        } else {
            if (last_non_exp_folder == null || last_non_exp_folder != hovered_item) {
                log_in_trial('on_nonexp_folder', {'folder': items[item_id].params.id, 'x': x, 'y': y});
                last_non_exp_folder = hovered_item;
            }
            items[item_id].fillColor = colors.inzoneblink;
            items[item_id].strokeColor = colors.outzoneblink;
        }
    } else {
        if (item_expanding_info.id != items[item_id].params.id) { no_item(x, y); }
    }
}

function expanding(item_id) {
    if (item_id < 0) { return; }
    let iid = items[item_id].params.id;

    let pid = -1
    if (iid % 10 != 0) {
        pid = item_id - 1;
        while (pid != -1 && items[pid].params.id % 10 != 0) {
            pid--;
        }
    }

    let ppid = -1
    if (iid % 100 != 0) {
        ppid = item_id - 1;
        while (ppid != -1 && items[ppid].params.id % 100 != 0) {
            ppid--;
        }
    }

    if (ppid != -1) { expanding(ppid); }
    if (pid != -1) { expanding(pid); }

    let offset = 0;
    let unfolding = false;
    for (let k = 0; k < items.length; k++) {
        if (unfolding) {
            if (items[k].params.hierachy == items[item_id].params.hierachy+1) {
                offset += items[k].params.size[1];
            } else if (items[k].params.hierachy == items[item_id].params.hierachy) {
                unfolding = false;
            }
        }

        items[k].params.pos[1] += offset;
        items[k].translate([0,offset]);
        labels[k].translate([0,offset]);

        if (items[k].params.id == items[item_id].params.id) {
            unfolding = true;
            items[k].params.exp = true;
            labels[k].content = labels[k].content.replace('▶', '▼')
        }
    }

    log_in_trial('exp_folder', {'folder': items[item_id].params.id});

    update_scene()
}

function no_item(x, y) {
    if (item_expanding_info != null) {
        log_in_trial('out_folder', {'folder': item_expanding_info.id, 'x': x, 'y': y});
        items[item_expanding_info.k].fillColor = colors.inzone;
        items[item_expanding_info.k].strokeColor = colors.outzone;
        clearInterval(item_expanding_info.blink);
        clearTimeout(item_expanding_info.handle);
        item_expanding_info = null;
    }
    for (let k = 0; k < items.length; k++) {
        items[k].fillColor = colors.inzone;
        items[k].strokeColor = colors.outzone;
    }
}

function collapse_all() {
    for (let k = 0; k < items.length; k++) {
        if (items[k].params.sublist.length) {
            items[k].params.exp = false;
        }

        let dy = items[k].params.opos[1] - items[k].params.pos[1];
        labels[k].translate([0,dy]);
        items[k].translate([0,dy]);
        items[k].params.pos[1] = items[k].params.opos[1];
        labels[k].content = labels[k].content.replace('▼', '▶')
    }

    update_scene()
}

function move_back(shape, pos, duration=100) {
    shape.tween({
        'position.x': pos[0],
        'position.y': pos[1]
    },{
        duration: duration,
        easing: 'easeInCubic'
    });
    collapse_all();
}

function drop_box(id, level, x, y) {
    for (let l = 0; l < boxes.length; l++) {
        if (boxes[l].box.params.id == id) {
            let on_box = null;
            for (let k = 0; k < items.length; k++) {
                if (inside(x, y, items[k].params.pos, items[k].params.size)) {
                    if (on_box == null) {
                        on_box = k;
                    } else if (items[on_box].params.id > items[k].params.id) {
                        on_box = k;
                    }
                }
            }
            if (on_box != null) {
                if (boxes[l].box.params.target == items[on_box].params.id) {
                    boxes[l].box.params.level = items[on_box].params.goto;
                    no_item(x, y);
                    trial_done();
                    return;
                }
            }
            boxes[l].box.params.pos[0] = boxes[l].box.params.opos[0];
            boxes[l].box.params.pos[1] = boxes[l].box.params.opos[1];
            move_back(boxes[l].box, [boxes[l].box.params.opos[0]+boxes[l].box.params.size[0]*0.5, boxes[l].box.params.opos[0]+boxes[l].box.params.size[1]*0.5]);
            if (boxes[l].label != null) {
                let _h = boxes[l].label.getBounds().getHeight();
                let _pos = boxlabel_position(boxes[l].box.params);
                move_back(boxes[l].label, [_pos[0], _pos[1]-_h*0.25,]);
            }
            no_item(x, y);
        }
    }
}

function boxlabel_position(params) {
    return [params.opos[0] + params.size[0]*0.5, params.opos[1] + params.size[1]*0.5 + 5];
}

function box(params) {
    let out = 1;
    let box = {};
    
    let rect =  new paper.Path.Rectangle({
        point: [params.opos[0]+out*0.5, params.opos[1]+out*0.5],
        size: [params.size[0]-out, params.size[1]-out],
        fillColor: '#353535',
        strokeColor: colors.outbox,
        strokeWidth: out
    });
    box.ghost = rect;
    
    let label = null;
    if (params.label !== undefined) {
        label = new paper.PointText({
            point: boxlabel_position(params),
            fillColor: 'white',
            fontSize: '13px',
            fontFamily: 'monospace',
            justification: 'center',
            content: params.label
        });
    }
    box.ghost_label = label;

    rect =  new paper.Path.Rectangle({
        point: [params.opos[0]+out*0.5, params.opos[1]+out*0.5],
        size: [params.size[0]-out, params.size[1]-out],
        fillColor: colors.inbox,
        strokeColor: colors.outbox,
        strokeWidth: out,
        params: params
    });
    box.box = rect;

    // rect =  new paper.Path.Rectangle({
    //     point: [params.opos[0] + params.size[0]*0.8, params.opos[1] + params.size[1]*(1/7)],
    //     size: [params.size[0]*0.1, params.size[1]*(1/7)],
    //     fillColor: colors.boxlabel,
    //     strokeWidth: 0,
    // });
    // box.grab1 = rect;
    // rect =  new paper.Path.Rectangle({
    //     point: [params.opos[0] + params.size[0]*0.8, params.opos[1] + params.size[1]*(3/7)],
    //     size: [params.size[0]*0.1, params.size[1]*(1/7)],
    //     fillColor: colors.boxlabel,
    //     strokeWidth: 0,
    // });
    // box.grab2 = rect;
    // rect =  new paper.Path.Rectangle({
    //     point: [params.opos[0] + params.size[0]*0.8, params.opos[1] + params.size[1]*(5/7)],
    //     size: [params.size[0]*0.1, params.size[1]*(1/7)],
    //     fillColor: colors.boxlabel,
    //     strokeWidth: 0,
    // });
    // box.grab3 = rect;
    rect =  new paper.Path.Rectangle({
        point: [params.opos[0] + params.size[0]*0.8, params.opos[1] + params.size[1]*(1/9)],
        size: [params.size[0]*0.1, params.size[1]*(1/9)],
        fillColor: colors.boxlabel,
        strokeWidth: 0,
    });
    box.grab1 = rect;
    rect =  new paper.Path.Rectangle({
        point: [params.opos[0] + params.size[0]*0.8, params.opos[1] + params.size[1]*(3/9)],
        size: [params.size[0]*0.1, params.size[1]*(1/9)],
        fillColor: colors.boxlabel,
        strokeWidth: 0,
    });
    box.grab2 = rect;
    rect =  new paper.Path.Rectangle({
        point: [params.opos[0] + params.size[0]*0.8, params.opos[1] + params.size[1]*(5/9)],
        size: [params.size[0]*0.1, params.size[1]*(1/9)],
        fillColor: colors.boxlabel,
        strokeWidth: 0,
    });
    box.grab3 = rect;
    rect =  new paper.Path.Rectangle({
        point: [params.opos[0] + params.size[0]*0.8, params.opos[1] + params.size[1]*(7/9)],
        size: [params.size[0]*0.1, params.size[1]*(1/9)],
        fillColor: colors.boxlabel,
        strokeWidth: 0,
    });
    box.grab4 = rect;
    
    label = null;
    if (params.label !== undefined) {
        label = new paper.PointText({
            point: boxlabel_position(params),
            fillColor: colors.boxlabel,
            fontSize: '13px',
            fontFamily: 'monospace',
            justification: 'center',
            content: params.label
        });
    }
    box.label = label;
    
    boxes.push(box);
}

function string_hierachy(hierachy, expanded) {
    let end = '  ';
    if (!expanded) { end = '▶'+end; } else { end += ' '; }
    switch(hierachy) {
        case 1:
            return end;
        case 2:
            return ' | '+end;
        case 3:
            return ' │ | '+end;
        case 4:
            return ' │ │ | '+end;
        default:
            return '';
    }
}

function listlabel_position(params) {
    return [params.opos[0] + 10, params.opos[1] + params.size[1]*0.5 + 5];
}

function list(params, top=true) {
    let _items = [];
    let _labels = [];

    for (let k = 0; k < params.sublist.length; k++) {
        let res = list(params.sublist[k], false);
        _items  = _items.concat(res[0]);
        _labels = _labels.concat(res[1]);
    }

    let out = 1;
    let rect =  new paper.Path.Rectangle({
        point: [params.opos[0]+out*0.5, params.opos[1]+out*0.5],
        size: [params.size[0]-out, params.size[1]-out],
        fillColor: colors.inzone,
        strokeColor: colors.outzone,
        strokeWidth: out,
        params: params
    });
    let label = new paper.PointText({
        point: listlabel_position(params),
        fillColor: colors.label,
        fontSize: '13px',
        fontFamily: 'monospace',
        justification: 'left',
        content: string_hierachy(params.hierachy, params.exp)+params.label
    });

    _items = [rect].concat(_items);
    _labels = [label].concat(_labels);

    if (!top) {
        return [_items, _labels];
    } else {
        items = items.concat(_items);
        labels = labels.concat(_labels);
    }
}

function _button(text, x, y , w, h) {
    let rect =  new paper.Path.Rectangle({
        point: [x-1, y-1],
        size: [w, h],
        strokeColor: 'black',
        strokeWidth: 2,
        fillColor: colors.button
    });

    let label = new paper.PointText({
        point: [x + w*0.5, y + h*0.5 + 5],
        fillColor: colors.butlabel,
        fontSize: '15px',
        fontFamily: 'monospace',
        justification: 'center',
        content: text
    });

    return [rect, label];
}

function button(text, pos, callback) {
    let res = null
    let w = paper.view.size.getWidth();
    let h = paper.view.size.getHeight();

    if (pos == 'center') {
        res = _button(text, w * 0.25, h * 0.45, w * 0.5, h * 0.1);
    } else {
        res = _button(text, w * 0.25, h * 0.25, w * 0.5, h * 0.5);
    }
    let but = res[0];
    but.onClick = callback;
    scene.push(but);

    let label = res[1];
    label.onClick = callback;
    scene.push(label);
}