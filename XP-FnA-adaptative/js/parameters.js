function get_nb_sublevels(k2) {
    switch(k2) {
        case 10:
            return 4;
        case 20:
            return 3;
        case 30:
            return 2;
        case 40:
            return 1;
        case 50:
            return 5;
    }
}

class Parameters {

    constructor() {
        this._dwell = undefined;
        this.mcts = [baseline, baseline, baseline]; // at least 3 examples with the baseline
        this.cur_mct = null;
        this.last_mct = baseline;
    }

    set dwell(val) { this._dwell = val; }
    get dwell() {
        if (this._dwell === undefined) {
            if (this.cur_mct == null) { this.cur_mct = this.compute_mean_mct(); }
            this.last_mct = this.cur_mct;
            console.log(`Dwell used : ${this.cur_mct}ms -- dynamic`);
            return this.cur_mct;
        } else {
            console.log(`Dwell used : ${this._dwell}ms -- static`);
            return this._dwell;
        }
    }

    get_label(level) {
        let first  = Math.floor(level / 100);
        let second = Math.floor((level - first*100) / 10);
        let third  = (level - first*100 - second*10);
        if (third == 0) {
            if (second == 0) {
                return `${first}`;
            } else {
                return `${first}.${second}`;
            }
        }
        return `${first}.${second}.${third}`;
    }

    dragndrop_list(type, target) {
        let h = 20;
        let w = 210;
        let ox = 5;
        let oy = ox+h+5;

        let levels = [];
        let targets = [];
        
        switch(type) {
            case 'xp':
                if (target % 100 != 0) {
                    for (let k1 = 100; k1 <= 1600; k1 += 100) {
                        levels.push(k1);
                        for (let k2 = 10; k2 <= 50; k2 += 10) {
                            levels.push(k1+k2);
                            let nbsubs = get_nb_sublevels(k2);
                            for (let k3 = 1; k3 <= nbsubs; k3 += 1) {
                                levels.push(k1+k2+k3);
                                targets.push(k1+k2+k3);
                            }
                        }
                    }
                    break;
                }
                type = 'fitts';
            case 'fitts':
                for (let k = 100; k <= 1600; k += 100) {
                    levels.push(k);
                    targets.push(k);
                }
                break;
        }

        // let target = targets[Math.floor(targets.length * Math.random())];
        let label = this.get_label(target);
        let box = { 'id': 1, 'level': 1, 'opos': [ox, ox], 'pos': [ox, ox], 'size': [w, h], 'label': `drop in ${label}`, 'target': target };

        let items = [];
        for (let k = 0; k < levels.length; k++) {
            let label = this.get_label(levels[k]);
            let depth = label.split('.');
            let item = {
                'id': levels[k], 'exp': true, 'hierachy': depth.length, 'goto':2,
                'opos': [ox, (parseInt(depth[0])-1)*h+oy], 'pos': [ox, (parseInt(depth[0])-1)*h+oy],
                'size': [w, h], 'label': label, 'sublist': []
            };
            switch (depth.length) {
                case 1:
                    if (type == 'xp') { item.exp = false; }
                    items.push(item);
                    break;
                case 2:
                    if (type == 'xp') { item.exp = false; }
                    items[(parseInt(depth[0])-1)].sublist.push(item);
                    // items[(parseInt(depth[0])-1)].exp = false;
                    break;
                case 3:
                    items[(parseInt(depth[0])-1)].sublist[(parseInt(depth[1])-1)].sublist.push(item);
                    // items[(parseInt(depth[0])-1)].sublist[(parseInt(depth[1])-1)].exp = false;
            }
        }


        return { 'target' : box, 'list' : items }
    }

    get_task() {
        if (this.current >= this.tasks.length) { return null; }
        let task = null;
        if (this.current == -1) {
            task = this.tasks[Math.floor(Math.random()*this.tasks.length)];
            document.getElementById('counter').innerHTML = ``;
        } else {
            task = this.tasks[this.current];
            this.current = this.current + 1;
            document.getElementById('counter').innerHTML = `${this.current}/${this.tasks.length}`;
        }
        return JSON.parse(JSON.stringify(task));
    }

    set_fitts_tasks() {
        this.tasks = [];

        // let odds = [100,300,500,700,900,1100,1300,1500];
        // odds = shuffle(odds);

        // for (let k = 0; k < 4; k++) {
        //     this.tasks.push({ 'type': 'fitts', 'dwell': this.dwell, 'value': this.dragndrop_list('fitts', odds[k]) })
        // }
        
        // let evens = [200,400,600,800,1000,1200,1400,1600];
        // evens = evens.concat(evens);
        // evens = shuffle(evens);

        // for (let k = 0; k < evens.length; k++) {
        //     this.tasks.push({ 'type': 'fitts', 'dwell': this.dwell, 'value': this.dragndrop_list('fitts', evens[k]) })
        // }
        let targets = [300,400,500,600,700,800,900,1000,1100,1200,1300,1400];
        targets = shuffle(targets);

        for (let k = 0; k < targets.length; k++) {
            this.tasks.push({ 'type': 'fitts', 'dwell': this.dwell, 'value': this.dragndrop_list('fitts', targets[k]) })
        }

        this.current = 0;
        if (DEBUG && !full_xp) { this.current = this.tasks.length-1; }
    }

    set_xp_example_tasks() {
        this.tasks = [];

        let odds = [155,355,555,755,955,1155,1355,1555];
        odds = shuffle(odds);

        for (let k = 0; k < training_nb_trials; k++) {
            this.tasks.push({ 'type': 'xp', 'dwell': this.dwell, 'value': this.dragndrop_list('xp', odds[k]) })
        }

        this.current = 0;
        if (DEBUG  && !full_xp) { this.current = this.tasks.length-1; }
    }

    set_xp_tasks() {
        this.tasks = [];

        let targets_h = [355,455,555,655,755,855,955,1055,1155,1255,1355,1455];
        targets_h = shuffle(targets_h);

        let targets_f = [200,300,400,500,600,700,800,900,1000,1100,1200,1300];
        targets_f = targets_f.concat(targets_f)
        targets_f = shuffle(targets_f);

        let targets = [];
        for (let k = 0; k < targets_h.length; k++) {
            targets.push(targets_f[2*k + 0]);
            targets.push(targets_f[2*k + 1]);
            targets.push(targets_h[k]);
        }

        for (let k = 0; k < targets.length; k++) {
            this.tasks.push({ 'type': 'xp', 'dwell': this.dwell, 'value': this.dragndrop_list('xp', targets[k]) })
        }

        this.current = 0;
        if (DEBUG  && !full_xp) { this.current = this.tasks.length-1; }
    }

    set_sandbox(limit = -1) {
        in_sandbox = true;
        at_least_once = false;

        this.tasks = [];

        let targets = [355,455,555,655,755,855,955,1055,1155,1255,1355,1455];
        targets = shuffle(targets);

        for (let k = 0; k < targets.length; k++) {
            this.tasks.push({ 'type': 'test', 'value': this.dragndrop_list('xp', targets[k]) })
        }

        this.current = -1;
        if (limit > 0) {
            if (this.tasks.length - limit >= 0) {
                this.current = 0;
                this.tasks = this.tasks.slice(0,limit)
            }
        }
    }

    compute_last_mct() {
        let k = logs.length - 1;
        let tboxin = null;
        let tdrop = null;

        while ((k >= 0) && ((tboxin == null) || (tdrop == null))) {
            if (logs[k].type == 'intrial') {
                if ((tdrop == null) && (logs[k].value.type == 'box_drop')) {
                    tdrop = logs[k].timestamp;
                }
                if ((tboxin == null) && (logs[k].value.type == 'on_nonexp_folder')) {
                    tboxin = logs[k].timestamp;
                }
            }
            k--;
        }
        if ((tboxin != null) && (tdrop != null)) {
            console.log(`dropped in ${tdrop - tboxin}ms`);
            this.mcts.push(tdrop - tboxin);
            this.cur_mct = null;
        }
    }

    compute_mean_mct() {
        // u_mct : user mean confirmation time (the time between entering the final target and dropping the object)
        let u_mct = 0;
        let nb_mct = 0;
        for (let k = this.mcts.length - mct_nb_trials; k < this.mcts.length; k++) {
            if (k < 0) { continue; }
            u_mct += (this.mcts[k] > MAX_ADAPT? MAX_ADAPT: (this.mcts[k] < MIN_ADAPT? MIN_ADAPT: this.mcts[k]));
            nb_mct++;
        }
        u_mct /= nb_mct;

        let delta = u_mct - this.last_mct;
        if (Math.abs(delta) > MAX_ADAPT_DELTA) {
            u_mct = this.last_mct + Math.sign(delta)*MAX_ADAPT_DELTA;
        }

        u_mct = (u_mct > MAX_ADAPT? MAX_ADAPT: u_mct);
        u_mct = (u_mct < MIN_ADAPT? MIN_ADAPT: u_mct);
        console.log(`New mct = ${u_mct}`);
        return u_mct;
    }

    defineAdaptativeDwell() {
        // if (adapt_dwell === undefined) {
        if (AdFi === undefined) {

            let u_mct = this.compute_mean_mct();
            // console.log(`u_mct = ${u_mct}`);
            // this.dwell = u_mct;

            // let z_u_mct = (u_mct - mean_conf) / sd_conf;
            // let adapted_timeout = mean_sys_c + (z_u_mct * sd_sys_c);
            // adapt_dwell = adapted_timeout;




            // if (u_mct <= 676) {
            //     user_category = 'fast';
            // } else if (u_mct <= 968) {
            //     user_category = 'middle';
            // } else {
            //     user_category = 'slow';
            // }

            // if (u_mct <= 599) {
            //     user_category = 'vfast';
            // } else if (u_mct <= 709) {
            //     user_category = 'fast';
            // } else if (u_mct <= 924) {
            //     user_category = 'middle';
            // } else if (u_mct <= 1172) {
            //     user_category = 'slow';
            // } else {
            //     user_category = 'vslow';
            // }
            // user_category = 'fast';
            // user cat not used anymore




            // cat_value = u_mct;

            // if (user_category == 'fast') {
            //     // adapt_dwell = baseline - baseline_delta;
            //     AdFi = users[user_category]['AdFi'] < users[user_category]['FiAd'];
            //     user_ord = (AdFi? 'AdFi' : 'FiAd');
            // } else if (user_category == 'slow') {
            //     // adapt_dwell = baseline + baseline_delta;
            //     AdFi = users[user_category]['AdFi'] < users[user_category]['FiAd'];
            //     user_ord = (AdFi? 'AdFi' : 'FiAd');
            // } else if (user_category == 'vfast') {
            //     // adapt_dwell = baseline - baseline_delta;
            //     AdFi = users[user_category]['AdFi'] < users[user_category]['FiAd'];
            //     user_ord = (AdFi? 'AdFi' : 'FiAd');
            // } else if (user_category == 'vslow') {
            //     // adapt_dwell = baseline + baseline_delta;
            //     AdFi = users[user_category]['AdFi'] < users[user_category]['FiAd'];
            //     user_ord = (AdFi? 'AdFi' : 'FiAd');
            // } else {
            //     if (users[user_category]['F-AdFi'] + users[user_category]['F-FiAd'] < users[user_category]['S-AdFi'] + users[user_category]['S-FiAd']) {
            //         // adapt_dwell = baseline - baseline_delta;
            //         AdFi = users[user_category]['F-AdFi'] < users[user_category]['F-FiAd'];
            //         user_ord = (AdFi? 'F-AdFi' : 'F-FiAd');
            //     } else {
            //         // adapt_dwell = baseline + baseline_delta;
            //         AdFi = users[user_category]['S-AdFi'] < users[user_category]['S-FiAd'];
            //         user_ord = (AdFi? 'S-AdFi' : 'S-FiAd');
            //     }
            // }

            cat_value = u_mct;

            let nbdyn = users['dynamic']['AdFi'] + users['dynamic']['FiAd'];
            let nbsta = 0;
            nbsta += users['static-f']['AdFi'] + users['static-f']['FiAd'];
            nbsta += users['static-s']['AdFi'] + users['static-s']['FiAd'];
            nbsta += users['static-m']['F-AdFi'] + users['static-m']['F-FiAd'] + users['static-m']['S-AdFi'] + users['static-m']['S-FiAd']

            if (MODE_SLIDDING_WIN && MODE_FIXED_ADA_VAL) {
                if (nbdyn <= nbsta) {
                    user_category = 'dynamic';
                } else {
                    if (u_mct <= 676) {
                        user_category = 'static-f';
                    } else if (u_mct <= 968) {
                        user_category = 'static-m';
                    } else {
                        user_category = 'static-s';
                    }
                }
            } else if (MODE_SLIDDING_WIN && !MODE_FIXED_ADA_VAL) {
                user_category = 'dynamic';
            } else if (!MODE_SLIDDING_WIN && MODE_FIXED_ADA_VAL) {
                if (u_mct <= 676) {
                    user_category = 'static-f';
                } else if (u_mct <= 968) {
                    user_category = 'static-m';
                } else {
                    user_category = 'static-s';
                }
            } else {
                user_category = 'dynamic';
            }

            if (user_category == 'dynamic') {
                AdFi = users[user_category]['AdFi'] < users[user_category]['FiAd'];
                user_ord = (AdFi? 'AdFi' : 'FiAd');
            } else if (user_category == 'static-s') {
                adapt_dwell = baseline + baseline_delta;
                AdFi = users[user_category]['AdFi'] < users[user_category]['FiAd'];
                user_ord = (AdFi? 'AdFi' : 'FiAd');
            } else if (user_category == 'static-f') {
                adapt_dwell = baseline - baseline_delta;
                AdFi = users[user_category]['AdFi'] < users[user_category]['FiAd'];
                user_ord = (AdFi? 'AdFi' : 'FiAd');
            } else {
                if (users['static-m']['F-AdFi'] + users['static-m']['F-FiAd'] < users['static-m']['S-AdFi'] + users['static-m']['S-FiAd']) {
                    adapt_dwell = baseline - baseline_delta;
                    AdFi = users['static-m']['F-AdFi'] < users['static-m']['F-FiAd'];
                    user_ord = (AdFi? 'F-AdFi' : 'F-FiAd');
                } else {
                    adapt_dwell = baseline + baseline_delta;
                    AdFi = users['static-m']['S-AdFi'] < users['static-m']['S-FiAd'];
                    user_ord = (AdFi? 'S-AdFi' : 'S-FiAd');
                }
            }

            if (!DEBUG) {
                modify_item('user_id', user_db_ref, { id: user_id, batch: xpbatch, cat: user_category, ord: user_ord });
            } else {
                console.log({ id: user_id, batch: xpbatch, cat: user_category, ord: user_ord });
            }
            
        }
    }
}

var parameters = new Parameters();

var colors = {
    butlabel: '#FFFFFF',
    button:   '#2C66D2',

    inzone:       '#EEEEEE',
    inzoneblink:  '#5CA7EA',
    outzone:      '#7D7D7D',
    outzoneblink: '#3D3D9D',

    label: '#000000',
    boxlabel: '#FFFFFF',
    
    inbox:     '#2154AE',
    outbox:    '#242424',
    activeinbox: '#2154AEBB',
    activeinrightbox: '#68DC72BB',
    activeoutbox: '#242424BB',

    done: '#02881f',
    background: 'white'
}